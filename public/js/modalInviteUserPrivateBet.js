var modal = document.getElementById("modalInviteUserPrivateBet");
var modalConfirmPayment = document.getElementById("modalConfirmPayment");
var modalSoldTooSmall = document.getElementById("modalSoldTooSmall");

var btn = document.getElementById("openModalInviteUser");

var spanConfirmPayment = document.getElementsByClassName("close")[0];
var spanSoldTooSmall = document.getElementsByClassName("close")[1];
var spanSponso = document.getElementsByClassName("close")[2];

if (btn != null) {
  btn.onclick = function() {
    modal.style.display = "block";
  }
}

if (modal != null) {
    spanSponso.onclick = function() {
    modal.style.display = "none";
  }
}

if (modalSoldTooSmall != null) {
  spanSoldTooSmall.onclick = function() {
  modalSoldTooSmall.style.display = "none";
}
}

if (modalConfirmPayment != null) {
  spanConfirmPayment.onclick = function() {
    modalConfirmPayment.style.display = "none";
    var modal_body_remove = document.getElementById("modalPaymentBody");

    var modal_content = document.getElementById("modal-content");
    modal_content.removeChild(modal_body_remove);

    var cote_modal = document.getElementsByClassName("input-box");
    for (i = 0; i < cote_modal.length; i++) {
      cote_modal[i].disabled= false;
    }
  }
}


window.onclick = function(event) {
  if ((event.target == modal ) ) {
    modal.style.display = "none";
  }

  if ((event.target == modalSoldTooSmall ) ) {
    modalSoldTooSmall.style.display = "none";
  }
  
  if ((event.target == modalConfirmPayment ) ) {
    modalConfirmPayment.style.display = "none";
    var modal_body_remove = document.getElementById("modalPaymentBody");

    var modal_content = document.getElementById("modal-content");
    modal_content.removeChild(modal_body_remove);

    var cote_modal = document.getElementsByClassName("input-box");
    for (i = 0; i < cote_modal.length; i++) {
      cote_modal[i].disabled= false;
    }
    
      modalConfirmPayment.style.display = "none";
  }
}