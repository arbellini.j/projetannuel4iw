function amount() {
    var total_amount = 0;
    var btn_coin = document.getElementsByClassName("coin");
    var form = document.getElementById("payment-form");
    
    for(var i = 0; i < btn_coin.length; i++ ) {
        btn_coin[i].addEventListener("click", function(){
            if (document.getElementById("amount")){
                form.removeChild(document.getElementById("amount"))
            }
            
          
            var current = document.getElementsByClassName("selected_wallet");
  
            // Remplacement si exite déjà
            if (current.length > 0) {
              current[0].className = current[0].className.replace(" selected_wallet", "");
            }
        
            // Add the active class to the current/clicked button
            this.className += " selected_wallet";
            total_amount = this.textContent;
            total_amount *= 100;
           
            var input = document.createElement("input");
            input.setAttribute("type", "hidden");
            input.setAttribute("name", "amount");
            input.setAttribute("id", "amount");
            input.setAttribute("value", total_amount);
            form.appendChild(input);
        });
    }      
   return total_amount;
  }

  amount()