describe('SearchBar', function() {
    it('Test SearchBar', function() {
      cy.visit('http://127.0.0.1/')
      cy.get('#search_bar_search').type('foot{enter}')
      cy.get('.searchResults').contains('Football')
    })
})