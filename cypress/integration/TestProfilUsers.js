describe('Espace utilisateur - Modification', () => {
    it('Test affichage profil', () => {
        cy.visit('http://localhost/login');
        cy.get('form')
        cy.get('#inputEmail').type('user@user6.com')
        cy.get('#inputPassword').type('testest')
        cy.get('form').submit()

        cy.visit('http://localhost/user/390/edit') 
    })

    it('Test modification profil', () => {
        cy.visit('http://localhost/login');
        cy.get('#inputEmail').type('user@user6.com')
        cy.get('#inputPassword').type('testest{enter}')
        
        cy.visit('http://localhost/user/390/edit') 

        cy.get('input[type=file]').selectFile('public/images/avatar/img2.png')
        cy.get('.save-info').click()
    })

    it('Test suppression profil', () => {
        cy.visit('http://localhost/login');
        cy.get('#inputEmail').type('user@user6.com')
        cy.get('#inputPassword').type('testest{enter}')
        
        cy.visit('http://localhost/user/390/edit') 

        cy.get('.btn-delete').click()
    })

    it('Test connexion après suppression profil', () => {
        cy.visit('http://localhost/login');
        cy.get('#inputEmail').type('user@user6.com')
        cy.get('#inputPassword').type('testest{enter}')
    })
})