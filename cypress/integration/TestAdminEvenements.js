describe('Espace administrateur - CRUD Événements', () => {
    it('Test ajout événement', () => {
        cy.visit('http://localhost/admin/event/');
        cy.get('form')
        cy.get('#inputEmail').type('admin@gmail.com')
        cy.get('#inputPassword').type('password')
        cy.get('form').submit()

        cy.get('.admin-link-new-category').click()
        cy.get('#event_name').type('PSG vs OM')
        cy.get('#event_description').type('Match de foot prévu pour le 30/03/2022')
        cy.get('#event_date_date_day').select('30').should('have.value', '30')
        cy.get('#event_date_date_month').select('mars').should('have.value', '3')
        cy.get('#event_date_date_year').select('2022').should('have.value', '2022')
        cy.get('#event_date_time_hour').select('15').should('have.value', '15')
        cy.get('#event_date_time_minute').select('30').should('have.value', '30')
        cy.get('#event_category_idCategory').select('Foot').should('have.value', '33')
        cy.get('.btn').click({ force: true })
        cy.get('#table').contains('PSG vs OM')
    })

    it('Test modification événement', () => {
        cy.visit('http://localhost/admin/event/')
        cy.get('#inputEmail').type('user@user1.com')
        cy.get('#inputPassword').type('testest{enter}')

        cy.visit('http://localhost/admin/event/121/edit')
        cy.get('#event_description').clear().type('Match très important ! Pariez vite')
        cy.get('.btn').click({ force: true })
        cy.get('#table').contains('Match très important ! Pariez vite')
    })

    it('Test affichage événement', () => {
        cy.visit('http://localhost/admin/event/121')
        cy.get('#inputEmail').type('user@user1.com')
        cy.get('#inputPassword').type('testest{enter}')

        cy.get('.h2-show-admin-category').contains('PSG vs OM') // a push sur git
    })

    it('Test suppression événement', () => {
        cy.visit('http://localhost/admin/event/121/edit')
        cy.get('#inputEmail').type('user@user1.com')
        cy.get('#inputPassword').type('testest{enter}')

        cy.get('.btn-delete').click()
        cy.get('#table').not('PSG vs OM')
    })

    
})
