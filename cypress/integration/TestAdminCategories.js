describe('Espace administrateur - CRUD Catégories', function() {

    it('Test ajout catégorie', function() {
        //Login
        cy.visit('http://localhost/admin/category/')
        cy.get('#inputEmail').type('keligbrindeau@gmail.com')
        cy.get('#inputPassword').type('password{enter}')

        cy.get('.admin-link-new-category').click()
        cy.get('#category1_name').type('CypressTest')
        cy.get('.btn').click()
        
        if(cy.get('#table').not('CypressTest')){
            cy.get('.dynatable-page-next').click()
        }

        cy.get('#table').contains('CypressTest')
    })

    it('Test modification catégorie', function() {
        //Login
        cy.visit('http://localhost/admin/category/')
        cy.get('#inputEmail').type('keligbrindeau@gmail.com')
        cy.get('#inputPassword').type('password{enter}')

        cy.visit('http://localhost/admin/category/121/edit')
        cy.get('#category1_name').clear().type('CypressModification{enter}')

        if(cy.get('#table').not('CypressModification')){
            cy.get('.dynatable-page-next').click()
        }

        cy.get('#table').contains('CypressModification')
    })
    
    it('Test affichage info catégorie', function() {
        //Login
        cy.visit('http://localhost/admin/category/')
        cy.get('#inputEmail').type('keligbrindeau@gmail.com')
        cy.get('#inputPassword').type('password{enter}')

        cy.visit('http://localhost/admin/category/121')
        cy.get('.h2-show-admin-category').contains('CypressModification')
    })

    it('Test suppression catégorie', function() {
        //Login
        cy.visit('http://localhost/admin/category/')
        cy.get('#inputEmail').type('keligbrindeau@gmail.com')
        cy.get('#inputPassword').type('password{enter}')

        cy.visit('http://localhost/admin/category/121/edit')
        cy.get('.btn-delete').click()
        cy.get('#table').not('CypressModification')
    })

    
})