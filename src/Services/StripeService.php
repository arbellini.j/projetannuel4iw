<?php

namespace App\Services;

class StripeService {
    private $privateKey;

    public function __construct() {
        $this->privateKey = $_ENV['STRIPE_SK'];
    }

    public function paymentIntent() {
        \Stripe\Stripe::setApiKey($this->privateKey);
        
        return \Stripe\PaymentIntent::create([
            
            'amount' => $_SESSION['amount'],
            'currency' => 'eur',
            'automatic_payment_methods' => [
                'enabled' => true,
            ]
        ]);
    }

    public function payment(array $stripeParameter) {
        \Stripe\Stripe::setApiKey($this->privateKey);
        $payment_intent = null;

        if(isset($stripeParameter['stripeIntentId'])) {
            $payment_intent = \Stripe\PaymentIntent::retrieve($stripeParameter['stripeIntentId']);
        }
        
        if(!($stripeParameter['redirect_status'] === 'succeeded')) {
            $payment_intent->cancel();
        }

        return $payment_intent;
    }

    public function stripe(array $stripeParameter) {
        return $this->payment($_SESSION['amount'], 'eur', $stripeParameter);
    }
}