<?php

namespace App\Controller;

use App\Entity\Category;
use App\Form\Category1Type;
use App\Repository\CategoryRepository;
use App\Repository\EventRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Security\Voter\AdminVoter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

#[Route('/admin/category')]
class AdminCategoryController extends AbstractController
{
    #[Route('/', name: 'admin_category_index', methods: ['GET'])]
    #[IsGranted('ROLE_ADMIN')]
    public function index(CategoryRepository $categoryRepository): Response
    {
        return $this->render('admin_category/index.html.twig', [
            'categories' => $categoryRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'admin_category_new', methods: ['GET','POST'])]
    #[IsGranted('ROLE_ADMIN')]
    public function new(Request $request): Response
    {
        $category = new Category();
        $form = $this->createForm(Category1Type::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($category);
            $category->setCreatedAt(new \DateTimeImmutable('now'));
            $entityManager->flush();

            return $this->redirectToRoute('admin_category_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin_category/new.html.twig', [
            'category' => $category,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'admin_category_show', methods: ['GET'])]
    #[IsGranted(AdminVoter::VIEW, 'category')]
    public function show(Category $category, EventRepository $eventRepository, $id): Response
    {
        $this->denyAccessUnlessGranted(AdminVoter::VIEW, $category);

        return $this->render('admin_category/show.html.twig', [
            'category' => $category,
            'id' => $id,
            'events' => $eventRepository->findByCategoryIdCategory($id)
        ]);
    }

    #[Route('/{id}/edit', name: 'admin_category_edit', methods: ['GET','POST'])]
    #[IsGranted(AdminVoter::EDIT, 'category')]
    public function edit(Request $request, Category $category): Response
    {
        $this->denyAccessUnlessGranted(AdminVoter::EDIT, $category);
        
        $form = $this->createForm(Category1Type::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_category_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin_category/edit.html.twig', [
            'category' => $category,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'admin_category_delete', methods: ['POST'])]
    #[IsGranted(AdminVoter::DELETE, 'category')]
    public function delete(Request $request, Category $category, EventRepository $eventRepository, $id): Response
    {
        $this->denyAccessUnlessGranted(AdminVoter::DELETE, $category);
        if ($this->isCsrfTokenValid('delete'.$category->getId(), $request->request->get('_token'))) {
            $eventToSet = $eventRepository->findBy(['categoryIdCategory' => $id]);

            foreach($eventToSet as $events){
                $events->setCategoryIdCategory(null);
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($category);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin_category_index', [], Response::HTTP_SEE_OTHER);
    }
}
