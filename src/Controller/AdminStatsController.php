<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\BetRepository;
use App\Entity\Bet;
use App\Security\Voter\AdminVoter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;


class AdminStatsController extends AbstractController
{
    #[Route('/admin/statistiques', name: 'app_admin_stats')]
    #[IsGranted('ROLE_ADMIN')]
    public function index(BetRepository $betRepository): Response
    {
        //Graph pour comparer le nombre de paris normaux et de paris premiums
        $normalBets = $betRepository->findBy(['isSponsored'=>'false']);
        $premiumBets = $betRepository->findBy(['isSponsored'=>'true']);

        $countNormalBets = [count($normalBets)];
        $countPremiumBets = [count($premiumBets)];
    
        $graphValuesPieTypeBets = array_merge($countNormalBets, $countPremiumBets);

        //Graph pour comparer le nombre de paris privés et le nombre de paris publics
        $privateBets = $betRepository->findBy(['isPublic'=>'false']);
        $publicBets = $betRepository->findBy(['isPublic'=>'true']);

        $countPrivateBets = [count($privateBets)];
        $countPublicBets = [count($publicBets)];

        $graphValuesPieIsPublicBet = array_merge($countPrivateBets, $countPublicBets);

        //Graph pour comparer les paris ayant différents status
        $validatedBets = $betRepository->findBy(['status'=>'VALIDÉ']);
        $endedBets = $betRepository->findBy(['status'=>'Terminé']);
        $refusedBets = $betRepository->findBy(['status'=>'REFUSÉ']);
        $inProgressBets = $betRepository->findBy(['status'=>'EN COURS']);
        
        $countValidatedBets = [count($validatedBets)];
        $countEndedBets = [count($endedBets)];
        $countRefusedBets = [count($refusedBets)];
        $countInProgressBets = [count($inProgressBets)];

        $graphValuesPieBetStatus = array_merge($countValidatedBets, $countEndedBets, $countRefusedBets, $countInProgressBets);

        //Graph linéaire pour afficher le nombre de paris crées par date
        $betPerDay = $betRepository->countByDate();

        $dates = [];
        $betCountPerDate = [];

        foreach($betPerDay as $bet){
            $dates[] = $bet['date_paris'];
            $betCountPerDate[] = $bet['count'];
        }

        return $this->render('admin_stats/index.html.twig', [
            'countTypeBets' => json_encode($graphValuesPieTypeBets),
            'countIsPublicBet' => json_encode($graphValuesPieIsPublicBet),
            'countBetStatus' => json_encode($graphValuesPieBetStatus),
            'betDates' => json_encode($dates),
            'betCountPerDate' => json_encode($betCountPerDate)
        ]);
    }
}
