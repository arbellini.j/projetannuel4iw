<?php

namespace App\Controller;

use App\Entity\Event;
use App\Form\BetChoiceType;
use App\Form\EventType;
use App\Repository\BetChoiceRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\BetChoice;


#[Route('/test')]
class BetChoiceController extends AbstractController
{
    #[Route('/', name: 'bet_choice')]
    public function index(BetChoice $betChoice): Response
    {
        return $this->render('bet_choice/index.html.twig', [
            'betchoice' =>$betChoice,
        ]);
    }

}
