<?php

namespace App\Controller;

use App\Repository\BetChoiceHasUserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\SearchBarType;
use App\Repository\CategoryRepository;
use App\Repository\EventRepository;
use App\Repository\BetRepository;
use Symfony\Component\HttpFoundation\Request;

#[Route('/evenements')]
class UserEventController extends AbstractController
{
    #[Route('/{id}', name: 'evenements', methods: ['GET'])]
    public function index(CategoryRepository $categoryRepository, BetChoiceHasUserRepository $AsUser, EventRepository $eventRepository, BetRepository $betRepository, Request $request, $id): Response
    {
        if ($this->getUser() != null) {
            $user = $this->getUser();
            $wallet = $user->getWallet();
            setcookie('wallet_amount', $wallet->getTotal(), ['secure' => false, 'samesite' => 'None' ]);
        } 
        date_default_timezone_set('Europe/Paris');

        $events = $eventRepository->findBy(['categoryIdCategory'=>$id, 'status'=>"EN COURS"]);
        $famousBets = $betRepository->findBy(['status'=>'VALIDÉ', 'isSponsored' => false, 'isPublic' => true], ['createdAt' => 'DESC']);
        $categories = $categoryRepository->findAll();
        $allStakes = $AsUser->findBy(['validated'=>'false']);
        // $allStakes = $allStakes
        $allStakeValues = [];
        $allGainValues = [];
        foreach($allStakes as $value ){
            array_push($allStakeValues, $value->getStake());
            array_push($allGainValues, $value->getRatingAtThisMoment()*$value->getStake());
        }
        $allStakeValues = array_sum($allStakeValues);
        $allGainValues = array_sum($allGainValues);
        
        return $this->render('user_event/index.html.twig', [
            'controller_name' => 'UserEventController',
            'asUser'=>$AsUser,
            'famousBets' => $famousBets,
            'categories' => $categories,
            'sumOfStakes' => $allStakeValues,
            'sumOfGain' => $allGainValues,
            'events' => $events
        ]);
    }
}
