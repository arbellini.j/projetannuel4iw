<?php

namespace App\Controller;

use App\Entity\Category;
use App\Form\CategoryType;
use App\Entity\User;
use App\Form\RegisterType;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\RedirectResponse;
use App\Form\ForgottenPassType;
use App\Form\ResetPasswordType;
use App\Security\AppCustomAuthenticator;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{   
    #[Route('/login', name: 'app_login', methods: ['GET','POST'])]
    public function login(AuthenticationUtils $authenticationUtils, AppCustomAuthenticator $appCustomAuthenticator, Request $request, ManagerRegistry $doctrine): Response
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('front_default');
        }

        if(strpos($_SERVER['REQUEST_URI'], "=")) {
            $uriExplode = explode("=", $_SERVER['REQUEST_URI']);
            $uriFinal = $uriExplode[1];
        } 
        else {
            $uriFinal = "";
        }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
  
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();
        
        // get is user account is verified
        if (isset($_SESSION['isVerif']) && $_SESSION['isVerif'] === false) {
            $error_verified = "error account not verified";
            unset($_SESSION['iVerif']);
        }

        return $this->render('security/login.html.twig', [
            'last_username' => $lastUsername,
             'error' => $error,
             'error_verified' => $error_verified ?? null,
             'back_to_your_page' => $uriFinal,
            ]);
    }


    #[Route('/logout', name: 'app_logout', methods: ['GET'])]
    public function logout(): void
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }

    
    #[Route('/forget-pass', name: 'app_forgotten_password', methods: ['GET','POST'])]
    public function forgetPass(Request $request, UserRepository $users, TokenGeneratorInterface $tokenGenerator, MailerInterface $mailer): Response
    {
        $form = $this->createForm(ForgottenPassType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $donnees = $form->getData();
            $user = $users->findOneByEmail($donnees['email']);
            if ($user === null) {
                $this->addFlash('message', 'Si votre compte existe, un mail vous sera envoyé !');
                
                return $this->redirectToRoute('app_login');
            }

            $token = $tokenGenerator->generateToken();

            try {
                $user->setResetToken($token);
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($user);
                $entityManager->flush();
            } catch (\Exception $e) {
                $this->addFlash('warning', $e->getMessage());
                return $this->redirectToRoute('app_login');
            }

            $url = $this->generateUrl('app_reset_password', array('token' => $token), UrlGeneratorInterface::ABSOLUTE_URL);
            
            $message = (new TemplatedEmail())
                ->from(new Address('teachr.contact.pa@gmail.com', 'Anybet'))
                ->to($user->getEmail())
                ->subject('Réinitialisation de votre mot de passe')
                ->htmlTemplate('security/send_mail_reset_password.html.twig')
                ->context([
                    'url' => $url,
                    'userFirstname' => $user->getFirstname()
                ]);

            $mailer->send($message);

            $this->addFlash('message', 'Si votre compte existe, un mail vous sera envoyé !');

            return $this->redirectToRoute('app_login');
        }

        return $this->render('security/forgotten_password.html.twig',['emailForm' => $form->createView()]);
    }

    #[Route('/reset_pass/{token}', name: 'app_reset_password', methods: ['GET','POST'])]
    public function resetPassword(Request $request, string $token, UserPasswordHasherInterface $userPasswordHasherInterface)
    {
        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['reset_token' => $token]);

        $formResetPassword = $this->createForm(ResetPasswordType::class, $user);
        $formResetPassword->handleRequest($request);

        if ($user === null) {
            $this->addFlash('danger', 'Token Inconnu');
            return $this->redirectToRoute('app_login');
        }

        if ($formResetPassword->isSubmitted() && $formResetPassword->isValid()) {
            $user->setResetToken(null);
            $user->setPassword(
            $userPasswordHasherInterface->hashPassword(
                    $user,
                    $formResetPassword->get('password')->getData()
                )
            );

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            $this->addFlash('message', 'Mot de passe mis à jour');

            return $this->redirectToRoute('app_login');
        } 

        return $this->render('security/reset_password.html.twig', [
            'token' => $token, 
            'resetPasswordForm' => $formResetPassword->createView()
        ]);

    }
}
