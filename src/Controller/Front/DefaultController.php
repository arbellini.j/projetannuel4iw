<?php

namespace App\Controller\Front;

use App\Repository\BetChoiceHasUserRepository;
use App\Repository\BetChoiceRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Form\SearchBarType;
use App\Repository\CategoryRepository;
use App\Repository\EventRepository;
use App\Repository\BetRepository;
use App\Repository\TransactionRepository;

class DefaultController extends AbstractController
{
    #[Route('/', name: 'default')]
    public function index(CategoryRepository $categoryRepository, BetChoiceHasUserRepository $AsUser, EventRepository $eventRepository, BetRepository $betRepository, BetChoiceRepository $betChoices, TransactionRepository $transactionRepository, Request $request): Response
    {
        date_default_timezone_set('Europe/Paris');

        $searchResultCat = null;
        $searchResultEvent = null;
        $searchResultBet = null;
        
        $form = $this->createForm(SearchBarType::class);
        $search = $form->handleRequest($request);
        
        if($form->isSubmitted() && $form->isValid()){
            $searchResultCat = $categoryRepository->search($search->get('search')->getData());
            $searchResultEvent = $eventRepository->searchEvent($search->get('search')->getData());
            $searchResultBet = $betRepository->searchBet($search->get('search')->getData());
           
        }
        $sponsoredBets = $betRepository->findBy(['status' => 'VALIDÉ', 'isSponsored' => true, 'isPublic' => true], ['createdAt' => 'DESC']);
        
        $famousBets = $betRepository->findBy(['status'=>'VALIDÉ', 'isSponsored' => false, 'isPublic' => true], ['createdAt' => 'DESC']);

        $allBets = $betRepository->findAll();

        $categories = $categoryRepository->findAll();

        $allStakes = $AsUser->findBy(['validated'=>'false']);
        // $allStakes = $allStakes
        $allStakeValues = [];
        $allGainValues = [];
        foreach($allStakes as $value ){
            array_push($allStakeValues, $value->getStake());
            array_push($allGainValues, $value->getRatingAtThisMoment()*$value->getStake());
        }
        $allStakeValues = array_sum($allStakeValues);
        $allGainValues = array_sum($allGainValues);


        $now = new \DateTime("now");
        foreach($allBets as $bet){

            $eventId = $bet->getEventIdEvent()->getId();

            $event = $eventRepository->findOneBy(["id"=>$eventId]);
            $eventDate = $event->getDate();
            
            if($now >= $eventDate){
                $bet->setStatus("Terminé");
                $event->setStatus("Terminé");
                $this->getDoctrine()->getManager()->flush();
            }
        }

        if ($this->getUser() != null) {
            $user = $this->getUser();
            $wallet = $user->getWallet();
            setcookie('wallet_amount', $wallet->getTotal(), ['secure' => false, 'samesite' => 'None' ]);
        }
 
    
        return $this->render('front/default/index.html.twig', [
            'searchResultCat' => $searchResultCat,
            'searchResultEvent' => $searchResultEvent,
            'searchResultBet' => $searchResultBet,
            'betChoices' => $betChoices,
            'asUser'=>$AsUser,
            'famousBets' => $famousBets,
            'sponsoredBets' => $sponsoredBets,
            'categories' => $categories,
            'sumOfStakes' => $allStakeValues,
            'sumOfGain' => $allGainValues,
            'form' => $form->createView()

        ]);
         
    }
}
