<?php

namespace App\Controller;

use App\Repository\BetChoiceHasUserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\CategoryRepository;
use App\Repository\EventRepository;
use App\Repository\BetRepository;
use Symfony\Component\HttpFoundation\Request;
use App\Form\SearchBarType;

class UserCategoriesController extends AbstractController
{
    #[Route('/categories', name: 'categories')]
    public function index(CategoryRepository $categoryRepository, EventRepository $eventRepository, BetRepository $betRepository, BetChoiceHasUserRepository $AsUser, Request $request): Response
    {
        $famousBets = $betRepository->findBy(['status'=>'VALIDÉ', 'isSponsored' => false, 'isPublic' => true], ['createdAt' => 'DESC']);


        if ($this->getUser() != null) {
            $user = $this->getUser();
            $wallet = $user->getWallet();
            setcookie('wallet_amount', $wallet->getTotal(), ['secure' => false, 'samesite' => 'None' ]);
        }

        $allStakes = $AsUser->findBy(['validated'=>'false']);
        // $allStakes = $allStakes
        $allStakeValues = [];
        $allGainValues = [];
        foreach($allStakes as $value ){
            array_push($allStakeValues, $value->getStake());
            array_push($allGainValues, $value->getRatingAtThisMoment()*$value->getStake());
        }
        $allStakeValues = array_sum($allStakeValues);
        $allGainValues = array_sum($allGainValues);
        
        return $this->render('user_categories/categories.html.twig', [
            'categories' => $categoryRepository->findAll(),
            'famousBets' => $famousBets,
            'asUser'=>$AsUser,
            'sumOfStakes' => $allStakeValues,
            'sumOfGain' => $allGainValues,
        ]);
    }
}
