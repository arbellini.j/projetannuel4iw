<?php

namespace App\Controller;

use App\Entity\BetChoice;
use App\Entity\BetChoiceHasUser;
use App\Entity\Category;
use App\Entity\Transaction;
use App\Entity\Bet;
use App\Entity\Wallet;
use App\Repository\BetChoiceHasUserRepository;
use App\Repository\BetChoiceRepository;
use App\Repository\BetRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
// use Symfony\Component\Debug\Debug;

class PanierController extends AbstractController
{



    #[Route('/panier', name: 'panier')]
    public function index(Request $request, BetChoiceHasUserRepository $chosenBet, BetChoiceRepository $choiceRepository, BetChoiceHasUserRepository $asUser, EntityManagerInterface $manager): Response
    {
        
        $user = $this->getUser();
        $data = $request->getContent();
        $data = json_decode($data, true);
        foreach ($data as $key=>$value){
            $mise = $chosenBet->findOneBy(['id_betChoice'=>$key, 'user_idUser'=>$user, 'validated'=>false]);
            $mise->setStake($value);
            $mise->setValidated(true);
            $manager->persist($mise);
            $manager->flush();
        }

        return $this->json($data);
    }


    /**
     * @Route("/{id}/addBet", name="addBet")
     *
     */

    public function addBet(BetChoice $betChoice, EntityManagerInterface $manager, BetChoiceHasUserRepository $chosenBet, BetChoiceRepository $choiceRepository, BetRepository $bet, $id, Request $request)
    {
        $listOfBets = [];
        $user = $this->getUser();
        
        $userId = $user->getId();
        $listIdChoices = $chosenBet->findBy(['user_idUser'=>$user]);
        $betName = $betChoice->getBet()->getName();
        $eventName = $betChoice->getBet()->getEventIdEvent()->getName();
        $betChoiceName = $betChoice->getName();
        $betChoiceRating = $betChoice->getRating();

        /*$chosenBet->findAll().getId();*/
        if ($chosenBet->findOneBy(['user_idUser'=>$user, 'id_betChoice'=>$id])) {
            $clickedChoice = $chosenBet->findOneBy(['user_idUser'=>$user, 'id_betChoice'=>$id]);


            $manager->remove($clickedChoice);
            $manager->flush();

            return $this->json(['code' => 200, 'Bet name' => $betName , 'Event name' => $eventName, 'BetChoice name' => $betChoiceName, 'status' => 'remove'/* , 'total_wallet' => $total_wallet */] , 200);
        }


        $addBetChoice = new BetChoiceHasUser();
        $addBetChoice->setIdBetChoice($betChoice)
            ->setUserIdUser($user)
            ->setStake('0')
            ->setRatingAtThisMoment($betChoice->getRating())
            ->setValidated(false);



        $manager->persist($addBetChoice);
        $manager->flush();
        return $this->json(['code' => 200, 'Bet name' => $betName, 'Event name' => $eventName, 'BetChoice name' => $betChoiceName, 'status' => 'add', 'rating' => $betChoiceRating], 200);
    }


      /**
     * @Route("/{id}/deleteBetChoice", name="deleteBetChoice")
     *
     */

    public function deleteBetChoice(BetChoice $betChoice, EntityManagerInterface $manager, BetChoiceHasUserRepository $chosenBet, BetChoiceRepository $choiceRepository, BetRepository $bet, $id, Request $request)
    {
        $user = $this->getUser();
        $betChoice = $chosenBet->findOneBy(['user_idUser'=>$user, 'id_betChoice'=>$id]);

            $manager->remove($betChoice);
            $manager->flush();
            return $this->json(['code' => 200, 'status' => 'remove'], 200);
        
   }

    #[Route('/stake', name: 'stake', methods: [ 'POST', 'HEAD']) ]
    public function validateStake(EntityManagerInterface $manager, BetChoiceHasUserRepository $chosenBet, MailerInterface $mailer, Request $request): Response
    {
        $user = $this->getUser();
        if(!array_key_exists('stake', $_POST)){

            return $this->redirect($_SERVER['HTTP_REFERER']);
        }
        $noneValidated = $chosenBet->findBy(['user_idUser'=>$user, 'validated'=>false]);
        $stake = $_POST['stake'];

        foreach ($noneValidated as $one){
            $one->setStake($stake[$one->getIdBetChoice()->getId()]);
            $manager->persist($one);
            $manager->flush();
        }
        return $this->redirect($_SERVER['HTTP_REFERER']);
         

    }

   

    


    

    #[Route('/validate', name: 'validateBet', methods: [ 'GET', 'HEAD']) ]
    public function validateBet(EntityManagerInterface $manager, BetChoiceHasUserRepository $chosenBet, MailerInterface $mailer, Request $request): Response
    {
        $user = $this->getUser();
        $wallet = $user->getWallet();
        $total_wallet = $wallet->getTotal();
        $listIdChoices = $chosenBet->findBy(['user_idUser'=>$user]);
        $noneValidated = $chosenBet->findBy(['user_idUser'=>$user, 'validated'=>false]);
        $amount_cart = 0;
        foreach ($noneValidated as $one){
            $amount_cart += $one->getStake();
        }
        if ($total_wallet >= $amount_cart) {

            foreach ($noneValidated as $one){
                //dd($one->getIdBetChoice()->getId());
                // $one->setStake($stake[$one->getIdBetChoice()->getId()]);
                $one->setValidated(true);
                $manager->persist($one);
                $manager->flush();
            }
            
            // On retire du montant de la cagnotte la somme payée
            $wallet->setTotal($total_wallet - $amount_cart);
            $manager->persist($wallet);

            // On ajoute cette transaction dans la table 
            $transaction = new Transaction();
            $transaction->setWalletIdWallet($wallet);
            $transaction->setAmount("-". $amount_cart);
            date_default_timezone_set('Europe/Paris');
            $transaction->setDateTime(new \DateTime('now'));
            $transaction->setReason("Paiement pari");
            
            $manager->persist($transaction);
            $manager->flush(); 

            $this->addFlash('success', 'Votre paiement a bien été pris en compte.');

            $userCreator = $chosenBet->findOneBy(['user_idUser'=>$user]);
            $idCreator = $userCreator->getIdBetChoice()->getBet()->getUserIdUserCreator()->getId();
            $creatorFirstname = $userCreator->getIdBetChoice()->getBet()->getUserIdUserCreator()->getFirstname();
            $creatorLastname = $userCreator->getIdBetChoice()->getBet()->getUserIdUserCreator()->getLastname();
            $creatorEmail = $userCreator->getIdBetChoice()->getBet()->getUserIdUserCreator()->getEmail();
            $creatorWallet = $userCreator->getIdBetChoice()->getBet()->getUserIdUserCreator()->getWallet();

            $numberCreateBetPerUser = $this->getDoctrine()
                    ->getRepository(Bet::class)
                    ->findCreateBetPerUser($idCreator);

            $totalWalletCreator = $creatorWallet->getTotal();

            if(sizeof($numberCreateBetPerUser) === 10) {
                $valueFreebet = 10;
            }
            elseif (sizeof($numberCreateBetPerUser) === 50) {
                $valueFreebet = 20;
            }
            else {
                $valueFreebet = 0;
            }
            
            if(sizeof($numberCreateBetPerUser) === 10 || sizeof($numberCreateBetPerUser) === 50) {
                $message = (new TemplatedEmail())
                ->from(new Address('teachr.contact.pa@gmail.com', 'Anybet'))
                ->to($creatorEmail)
                ->subject('Freebet !')
                ->context([
                    'firstname' => $creatorFirstname,
                    'lastname' => $creatorLastname,
                    'valueFreebet' => $valueFreebet,
                    'numberPerUser' => sizeof($numberCreateBetPerUser)
                ])
                ->htmlTemplate('freebets/freebetCreateBet.html.twig');

                $mailer->send($message);

                $creatorWallet->setTotal($totalWalletCreator + $valueFreebet);
                $manager->persist($creatorWallet);
                
                $transaction = new Transaction();
                $transaction->setWalletIdWallet($creatorWallet);
                $transaction->setAmount("+". $valueFreebet);
                date_default_timezone_set('Europe/Paris');
                $transaction->setDateTime(new \DateTime('now'));
                $transaction->setReason("Freebet");
                
                $manager->persist($transaction);
                $manager->flush(); 
            }

            $numberParticipeBetPerUser = $this->getDoctrine()
                ->getRepository(BetCHoiceHasUser::class)
                ->findParticipeBetPerUser($this->getUser()->getId());
            
            $participePerUser = 0;
            foreach($numberParticipeBetPerUser as $element) {
                $participePerUser += $element[1];
            }

            if($participePerUser === 10) {
                $valueFreebet = 10;
            }
            elseif ($participePerUser === 50) {
                $valueFreebet = 20;
            }
            else {
                $valueFreebet = 0;
            }

            if ($participePerUser === 10 || $participePerUser === 50) {
                $message = (new TemplatedEmail())
                ->from(new Address('teachr.contact.pa@gmail.com', 'Anybet'))
                ->to($user->getEmail())
                ->subject('Freebet !')
                ->context([
                    'firstname' => $this->getUser()->getFirstname(),
                    'lastname' => $this->getUser()->getLastname(),
                    'valueFreebet' => $valueFreebet,
                    'participePerUser' => $participePerUser
                ])
                ->htmlTemplate('freebets/freebetParticipeBet.html.twig');

                // On retire du montant de la cagnotte la somme payée
                $wallet->setTotal($total_wallet - $amount_cart);
                $manager->persist($wallet);

                // On ajoute cette transaction dans la table 
                $transaction = new Transaction();
                $transaction->setWalletIdWallet($wallet);
                $transaction->setAmount("-". $amount_cart);
                date_default_timezone_set('Europe/Paris');
                $transaction->setDateTime(new \DateTime('now'));
                $transaction->setReason("Paiement pari");
                
                $manager->persist($transaction);
                $manager->flush(); 

                $this->addFlash('success', 'Votre paiement a bien été pris en compte.');
                
                $userCreator = $chosenBet->findOneBy(['user_idUser'=>$user]);
                $idCreator = $userCreator->getIdBetChoice()->getBet()->getUserIdUserCreator()->getId();
                $creatorFirstname = $userCreator->getIdBetChoice()->getBet()->getUserIdUserCreator()->getFirstname();
                $creatorLastname = $userCreator->getIdBetChoice()->getBet()->getUserIdUserCreator()->getLastname();
                $creatorEmail = $userCreator->getIdBetChoice()->getBet()->getUserIdUserCreator()->getEmail();
                $creatorWallet = $userCreator->getIdBetChoice()->getBet()->getUserIdUserCreator()->getWallet();

                $numberCreateBetPerUser = $this->getDoctrine()
                        ->getRepository(Bet::class)
                        ->findCreateBetPerUser($idCreator);

                
                $totalWalletCreator = $creatorWallet->getTotal();

                if(sizeof($numberCreateBetPerUser) === 10) {
                    $valueFreebet = 10;
                }
                elseif (sizeof($numberCreateBetPerUser) === 50) {
                    $valueFreebet = 20;
                }
                else {
                    $valueFreebet = 0;
                }
                
                if(sizeof($numberCreateBetPerUser) === 10 || sizeof($numberCreateBetPerUser) === 50) {
                    $message = (new TemplatedEmail())
                    ->from(new Address('teachr.contact.pa@gmail.com', 'Anybet'))
                    ->to($creatorEmail)
                    ->subject('Freebet !')
                    ->context([
                        'firstname' => $creatorFirstname,
                        'lastname' => $creatorLastname,
                        'valueFreebet' => $valueFreebet,
                        'numberPerUser' => sizeof($numberCreateBetPerUser)
                    ])
                    ->htmlTemplate('freebets/freebetCreateBet.html.twig');

                    $mailer->send($message);

                    $creatorWallet->setTotal($totalWalletCreator + $valueFreebet);
                    $manager->persist($creatorWallet);
                    
                    $transaction = new Transaction();
                    $transaction->setWalletIdWallet($creatorWallet);
                    $transaction->setAmount("+". $valueFreebet);
                    date_default_timezone_set('Europe/Paris');
                    $transaction->setDateTime(new \DateTime('now'));
                    $transaction->setReason("Freebet");
                    
                    $manager->persist($transaction);
                    $manager->flush(); 
                }

                $numberParticipeBetPerUser = $this->getDoctrine()
                    ->getRepository(BetCHoiceHasUser::class)
                    ->findParticipeBetPerUser($this->getUser()->getId());
                
                $participePerUser = 0;
                foreach($numberParticipeBetPerUser as $element) {
                    $participePerUser += $element[1];
                }

                if($participePerUser === 10) {
                    $valueFreebet = 10;
                }
                elseif ($participePerUser === 50) {
                    $valueFreebet = 20;
                }
                else {
                    $valueFreebet = 0;
                }

                if ($participePerUser === 10 || $participePerUser === 50) {
                    $message = (new TemplatedEmail())
                    ->from(new Address('teachr.contact.pa@gmail.com', 'Anybet'))
                    ->to($user->getEmail())
                    ->subject('Freebet !')
                    ->context([
                        'firstname' => $this->getUser()->getFirstname(),
                        'lastname' => $this->getUser()->getLastname(),
                        'valueFreebet' => $valueFreebet,
                        'participePerUser' => $participePerUser
                    ])
                    ->htmlTemplate('freebets/freebetParticipeBet.html.twig');
    
                    $mailer->send($message);

                    $wallet->setTotal($total_wallet + $valueFreebet);
                    $manager->persist($wallet);

                    $transaction = new Transaction();
                    $transaction->setWalletIdWallet($wallet);
                    $transaction->setAmount("+". $valueFreebet);
                    date_default_timezone_set('Europe/Paris');
                    $transaction->setDateTime(new \DateTime('now'));
                    $transaction->setReason("Freebet");
                    
                    $manager->persist($transaction);
                    $manager->flush(); 

                    $this->addFlash('green', "Vous avez gagné un freebet. Retrouvez la somme dans votre cagnotte et dans l'historique des transactions");
                }
                
                // Suppression des cookies
                setcookie('amount_cart');
                setcookie("wallet_amount");
                // dd($_COOKIE);
                //return $this->redirectToRoute($_SERVER['HTTP_REFERER']); 
                return $this->redirectToRoute('app_wallet');

                }
                return $this->json(['code' => 200, 'message' => 'Le paiement a bien été pris en compte', 'solde' => 'ok'], 200);
        }else {
            return $this->json(['code' => 200, 'solde' => 'not ok'], 200);
        }
        
        
            
        


    }

}
