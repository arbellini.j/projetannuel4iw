<?php

namespace App\Controller;

use App\Entity\Transaction;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Wallet;
use App\Form\WalletType;
use App\Manager\WalletManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class WalletController extends AbstractController
{
    #[Route('profil/ma-cagnotte', name: 'app_profile_wallet', methods: ['GET','POST'])]
    public function index(Request $request): Response {

      $form = $this->createForm(WalletType::class);
      $form->handleRequest($request); 
      
      if ($form->isSubmitted() && $form->isValid()) {
        $_SESSION['amount'] = $request->request->get('amount');
        if (isset( $_SESSION['amount'] )) {
          $_SESSION['amount'] = $request->request->get('amount');
        } else {
          $this->addFlash('red', 'Veuillez sélectionner un montant');
          return $this->redirectToRoute('app_profile_wallet');
        }
        

        return $this->redirectToRoute('app_payment');
      } 
      if($this->getUser() != null){
        return $this->renderForm('profile/wallet.html.twig', [
          'form' => $form
        ]);
      }
      else{
        return $this->redirectToRoute("app_login");
      }
      
    }

    #[Route('paiement', name: 'app_payment', methods: ['GET','POST'])]
    public function payment(WalletManager $walletManager): Response
    { 
      session_start();

      if($this->getUser() != null){
        return $this->renderForm('stripe/index.html.twig', [
          'intentSecret' => $walletManager->intentSecret(),
          'amount' => $_SESSION['amount'],
          'amount_final' => $_SESSION['amount']/100,
          'user' => $this->getUser()
        ]);
      }
      else{
        return $this->redirectToRoute("app_login");
      }

    }

    #[Route('paiement/sponsoredBet', name: 'app_payment_sponsored_bet', methods: ['GET','POST'])]
    public function paymentBet(WalletManager $walletManager, EntityManagerInterface $manager, Request $request): Response
    { 
      $user = $this->getUser();
      $wallet = $user->getWallet();
      $total_wallet = $wallet->getTotal();
      $_SESSION['amount'] = 8000;
      $amount_cart = 80;

      $wallet->setTotal($total_wallet - $amount_cart);
      $manager->persist($wallet);

      if($this->getUser() != null){
        return $this->renderForm('sponsoring/stripe.html.twig', [
          'intentSecret' => $walletManager->intentSecret(),
          'amount' => $_SESSION['amount'],
          'user' => $this->getUser()
        ]);
      }
      else{
        return $this->redirectToRoute("app_login");
      }

    }

    /**
    * @Route("/payBet", name="app_pay_sponsored_bet", options={"expose"=true}, methods={"GET"})
    */
    public function paybet(Request $request, WalletManager $walletManager, EntityManagerInterface $em) {
      session_start();
      $user = $this->getUser();
      $amount = 80;
      $wallet = $user->getWallet();
      if($wallet != null){
        $total_wallet = $wallet->getTotal();
        if($total_wallet >= $amount){
          if($request->getMethod() === "GET") {
            if(null !== $_GET) {
              // Si l'user a deja créé une cagnotte
              if(!empty($user->getWallet())) {
                $wallet = $user->getWallet();
                $total = $wallet->getTotal() - $amount;
                $wallet->setTotal($total);
                $em->persist($wallet);
                $em->flush();

              } else {
                $wallet = new Wallet();
                $total = $amount;
                $walletManager->chargeWallet($wallet, $user, $total);
                
              }
              $transaction = new Transaction;
              $transaction->setWalletIdWallet($wallet);
              $transaction->setAmount("-". $amount);
              date_default_timezone_set('Europe/Paris');
              $transaction->setDateTime(new \DateTime('now'));
              $transaction->setReason("Paiement pari sponsorisé");
              
              $em->persist($transaction);
              $em->flush(); 

              $_SESSION['validate_payment'] = true;

              $this->addFlash('green', 'Paiement validé !');
              
              return $this->redirectToRoute('form_sponsoring_bet');
            }
          }
        } else{
          $_SESSION['validate_payment'] = false;
          return $this->redirectToRoute("app_profile_wallet");
        }
      }

      return $this->redirectToRoute('app_payment');
    }

    /**
    * @Route("/souscription", name="app_subscription", options={"expose"=true}, methods={"GET"})
    */
    public function subscription(Request $request, WalletManager $walletManager, EntityManagerInterface $em) {
      $user = $this->getUser();
      $amount = $_SESSION['amount'];

      if($request->getMethod() === "GET") {
        if(null !== $_GET) {
          // Si l'user a deja créé une cagnotte
          if(!empty($user->getWallet())) {
            $wallet = $user->getWallet();
            $total = $wallet->getTotal() + $amount/100;
            $wallet->setTotal($total);
            $em->persist($wallet);
            $em->flush();

          } else {
            $wallet = new Wallet();
            $total = $amount;
            $walletManager->chargeWallet($wallet, $user, $total);
            
          }
          $transaction = new Transaction;
          $transaction->setWalletIdWallet($wallet);
          $transaction->setAmount($amount/100);
          date_default_timezone_set('Europe/Paris');
          $transaction->setDateTime(new \DateTime('now'));
          $transaction->setReason("Rechargement cagnotte");
          
          $em->persist($transaction);
          $em->flush(); 
          
          $this->addFlash('green', 'Votre cagnotte a bien été rechargée');
          
          return $this->redirectToRoute('app_wallet');
        }
      }

      return $this->redirectToRoute('app_payment');
    }

    #[Route('/ma-cagnotte', name: 'app_wallet')]
    public function displayWallet() {
      $user = $this->getUser();
      $wallet = $user->getWallet();

      if(!empty($user->getWallet())) {
        $total = $wallet->getTotal();
      }
      else {
        $total = null;
      }
      
      if($this->getUser() != null){
        return $this->render('wallet/index.html.twig', [
          'total' => $total
        ]);
      }
      else{
        return $this->redirectToRoute("app_login");
      }

    }

}
