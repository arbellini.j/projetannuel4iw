<?php

namespace App\Controller;

use App\Form\SearchBarType;
use App\Repository\BetChoiceHasUserRepository;
use App\Repository\BetChoiceRepository;
use App\Repository\BetRepository;
use App\Repository\CategoryRepository;
use App\Repository\EventRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


#[Route('/bet')]

class UserBetController extends AbstractController
{
    #[Route('/{id}', name: 'user_bet', methods: ['GET'])]
    public function index(BetRepository $betRepo, EventRepository $eventRepo, BetChoiceRepository $betChoices, BetChoiceHasUserRepository $AsUser,Request $request, CategoryRepository $categoryRepository, EventRepository $eventRepository, $id): Response
    {
        if ($this->getUser() != null) {
            $user = $this->getUser();
            $wallet = $user->getWallet();
            setcookie('wallet_amount', $wallet->getTotal(), ['secure' => false, 'samesite' => 'None' ]);
        }
        
        $sponsoredBets = $betRepo->findBy([
            'event_idEvent' => $id, 
            'status' => 'VALIDÉ', 
            'isSponsored' => true
        ],
        [
            'createdAt' => 'DESC'
        ]
        );
        
        $normalBets = $betRepo->findBy([
            'event_idEvent' => $id,
            'status' => 'VALIDÉ', 
            'isSponsored' => false
        ],
        [
            'createdAt' => 'DESC'
        ]);

        $famousBets = $betRepo->findBy(['status'=>'VALIDÉ', 'isSponsored' => false, 'isPublic' => true], ['createdAt' => 'DESC']);
        $categories = $categoryRepository->findAll();
        $allStakes = $AsUser->findBy(['validated'=>'false']);
        // $allStakes = $allStakes
        $allStakeValues = [];
        $allGainValues = [];
        foreach($allStakes as $value ){
            array_push($allStakeValues, $value->getStake());
            array_push($allGainValues, $value->getRatingAtThisMoment()*$value->getStake());
        }
        $allStakeValues = array_sum($allStakeValues);
        $allGainValues = array_sum($allGainValues);

        return $this->render('user_bet/index.html.twig', [
            'sponsoredBets' => $sponsoredBets,
            'normalBets' => $normalBets,
            'event' =>$eventRepo->findOneBy(['id'=>$id]),
            'betChoices' => $betChoices,
            'famousBets' => $famousBets,
            'categories' => $categories,
            'sumOfStakes' => $allStakeValues,
            'sumOfGain' => $allGainValues,
            'asUser'=>$AsUser,
            'id' =>$id,
        ]);
    }


}
