<?php

namespace App\Controller;


use App\Entity\Bet;
use App\Entity\Event;
use App\Entity\Category;
use App\Entity\User;
use App\Entity\BetChoice;
use App\Entity\Transaction;
use App\Form\BetType;
use App\Form\EventType;
use App\Form\BetChoiceType;
use App\Form\AdminResetFilterType;
use App\Form\AdminBetFilterType;
use App\Form\ModerationButtonValidate;
use App\Form\ModerationButtonRefuse;
use App\Form\AdminSetWinChoice;
use App\Form\CreateSponsoredBetType;
use App\Repository\BetChoiceRepository;
use App\Repository\BetRepository;
use App\Repository\EventRepository;
use App\Repository\CategoryRepository;
use App\Repository\UserRepository;
use App\Repository\BetChoiceHasUserRepository;
use Doctrine\DBAL\Types\TextType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mime\Address;
use Symfony\Component\Serializer\Encoder\JsonEncode;
use App\Security\Voter\AdminVoter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

use function PHPUnit\Framework\isEmpty;

#[Route('/admin/bet')]

class AdminBetController extends AbstractController
{

    protected $faker;
    #[Route('/', name: 'admin_bet_index', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_ADMIN')]
    public function index(BetChoiceRepository $betChoices, BetRepository $bets, CategoryRepository $categories, EventRepository $events, UserRepository $users, Request $request ): Response
    {
        $formFilter = $this->createForm(AdminBetFilterType::class)->handleRequest($request);
        $formReset = $this->createForm(AdminResetFilterType::class)->handleRequest($request);

        if ($formFilter->isSubmitted()) {
            if($formFilter['status']->getData() == "VALIDÉ" || $formFilter['status']->getData() == "REFUSÉ" || $formFilter['status']->getData() == "EN COURS"){
                $data = $bets->findBy(
                    [
                        "status" => $formFilter['status']->getData()
                    ],
                    [
                        "createdAt" => "DESC"
                    ]

                );
            }
            elseif($formFilter['status']->getData() == "false"){
                $data = $bets->findBy(
                    [
                        "isPublic" => $formFilter['status']->getData()
                    ],
                    [
                        "createdAt" => "DESC"
                    ]
                );
            }
            elseif($formFilter['status']->getData() == "true"){
                $data = $bets->findBy(
                    [
                        "isSponsored" => $formFilter['status']->getData()
                    ],
                    [
                        "createdAt" => "DESC"
                    ]
                    );

            }
            else{
                $data = $bets->findBy([], ["createdAt" => "DESC"]);
            }
        }
        else{
            $data = $bets->findBy([], ["createdAt" => "DESC"]);
        }

        if ($formReset->isSubmitted()) {
            $data = $bets->findBy([], ["createdAt" => "DESC"]);
        }

        $allBets = $bets->findAll();
        $now = new \DateTime("now");
        
        foreach($allBets as $bet){
            $eventId = $bet->getEventIdEvent()->getId();
            $event = $events->findOneBy(["id"=>$eventId]);
            $eventDate = $event->getDate();

            if($now >= $eventDate){
                $event->setStatus("Terminé");
                $bet->setStatus("Terminé");
                $this->getDoctrine()->getManager()->flush();
            }
        }

        return $this->render('admin_bet/index.html.twig', [
            'bets' => $data,
            'betChoices' => $betChoices,
            'categories' => $categories,
            'events' => $events,
            'users' => $users,
            'formFilter' => $formFilter->createView(),
            'formReset' => $formReset->createView()

        ]);
    }


    #[Route('/new', name: 'admin_bet_new', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_ADMIN')]
    public function new(Request $request, EntityManagerInterface $entityManager, BetChoiceHasUserRepository $AsUser, EventRepository $eventRepository, BetRepository $betRepository, MailerInterface $mailer): Response
    {

        $bet = new Bet();
        $event = new Event();
        $user = new User();

        $form = $this->createForm(BetType::class, $bet)->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $bet->setCreatedAt(new \DateTime("now"));
            $bet->setUserIdUserCreator($this->getUser());
            $bet->setIsPublic($form['isPublic']->getData());
            $betChoicesList = $bet->getBetChoices();
            foreach ($betChoicesList as $betChoice){
                $betChoice->setRating("2");
            }

            if($form['isPublic']->getData() == false){
                $bet->setStatus("PRIVÉ");
            }
            
            if($form['event']->getData() != null){
                $event->setName($form['event']->getData());
                $event->setDescription($form['eventDescription']->getData());
                $event->setDate($form['eventDate']->getData());
                $event->setStatus("EN COURS");
                $this->getDoctrine()->getManager()->persist($event);
            }
            $this->getDoctrine()->getManager()->persist($bet);
            $this->getDoctrine()->getManager()->flush();
            $lastBet = $betRepository->findBy(array(),array('id'=>'DESC'),1,0); 
            $lastEvent = $eventRepository->findBy(array(),array('id'=>'DESC'),1,0);

            if($form['isPublic']->getData() == true){

                $message = (new TemplatedEmail())
                ->from(new Address('teachr.contact.pa@gmail.com', 'Anybet'))
                ->to('teachr.contact.pa@gmail.com')
                ->subject('Nouvelle demande de paris créee !')
                ->context([
                    'id' => $bet->getId()
                ])
                ->htmlTemplate('create_bet/mailNotifyAdmin.html.twig');

                $mailer->send($message);
            }

            if($lastBet[0]->getEventIdEvent() == null){

                $bet->setEventIdEvent($lastEvent[0]);
                $event->setCategoryIdCategory($form['categoryList']->getData());

                $this->getDoctrine()->getManager()->persist($bet);
                $this->getDoctrine()->getManager()->flush();

            }

            return $this->redirectToRoute("admin_bet_index");
        }

        

        return $this->render('admin_bet/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    #[Route('/new-premium', name: 'admin_bet_new_premium', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_ADMIN')]
    public function newPremiumBet(Request $request, EntityManagerInterface $entityManager, BetChoiceHasUserRepository $asUser, EventRepository $eventRepository, BetRepository $betRepository, MailerInterface $mailer, CategoryRepository $categoryRepository): Response
    {

        $bet = new Bet();
        $event = new Event();
        $user = new User();

        $form = $this->createForm(CreateSponsoredBetType::class, $bet)->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $bet->setCreatedAt(new \DateTime("now"));
            $bet->setUserIdUserCreator($this->getUser());
            $bet->setIsPublic(true);
            $bet->setIsSponsored(true);
            $bet->setColor($_POST["theme"]);
            $betChoicesList = $bet->getBetChoices();
            foreach ($betChoicesList as $betChoice){
                $betChoice->setRating("2");
            }

            
            if($form['event']->getData() != null){
                $event->setName($form['event']->getData());
                $event->setDescription($form['eventDescription']->getData());
                $event->setDate($form['eventDate']->getData());
                $event->setStatus("EN COURS");
                $this->getDoctrine()->getManager()->persist($event);
            }
            $this->getDoctrine()->getManager()->persist($bet);
            $this->getDoctrine()->getManager()->flush();
            $lastBet = $betRepository->findBy(array(),array('id'=>'DESC'),1,0); 
            $lastEvent = $eventRepository->findBy(array(),array('id'=>'DESC'),1,0);

            if($form['isPublic']->getData() == true){

                $message = (new TemplatedEmail())
                ->from(new Address('teachr.contact.pa@gmail.com', 'Anybet'))
                ->to('teachr.contact.pa@gmail.com')
                ->subject('Nouvelle demande de paris créee !')
                ->context([
                    'id' => $bet->getId()
                ])
                ->htmlTemplate('create_bet/mailNotifyAdmin.html.twig');

                $mailer->send($message);
            }

            if($lastBet[0]->getEventIdEvent() == null){

                $bet->setEventIdEvent($lastEvent[0]);
                $event->setCategoryIdCategory($form['categoryList']->getData());

                $this->getDoctrine()->getManager()->persist($bet);
                $this->getDoctrine()->getManager()->flush();

            }
            
            return $this->redirectToRoute("admin_bet_index");
        }

        return $this->render('admin_bet/new-premium.html.twig', [
            'form' => $form->createView(),
        ]);
    }


    #[Route('/{id}', name: 'admin_bet_show', methods: ['GET', 'POST'])]
    #[IsGranted(AdminVoter::VIEW, 'bet')]
    public function show(BetRepository $betRepo, BetChoiceRepository $betChoices ,BetChoiceHasUserRepository $betChoiceHasUser,EventRepository $eventRepo, UserRepository $user, Request $request, EntityManagerInterface $manager, MailerInterface $mailer, $id, Bet $bet): Response
    {
        $this->denyAccessUnlessGranted(AdminVoter::VIEW, $bet);

        date_default_timezone_set('Europe/Paris');

        $bet = new Bet();
        $betChoice = new BetChoice();
    
        $formValidate = $this->createForm(ModerationButtonValidate::class, $bet)->handleRequest($request);
        $formRefuse = $this->createForm(ModerationButtonRefuse::class, $bet)->handleRequest($request);
        $formWinChoice = $this->createForm(AdminSetWinChoice::class, $betChoice, array('id'=>$id))->handleRequest($request);

        $userMail = $betRepo->findBy(["id"=>$id]);
        
        $betToUpdate = $manager->getRepository(Bet::class)->find($id);

        $isPublic = $betToUpdate->getIsPublic();

        if ($formValidate->isSubmitted()) {
            $betToUpdate->setStatus($formValidate['status']->getData());
    
            $this->getDoctrine()->getManager()->persist($betToUpdate);
            $this->getDoctrine()->getManager()->flush();

            $message = (new TemplatedEmail())
            ->from(new Address('teachr.contact.pa@gmail.com', 'Anybet'))
            ->to($userMail[0]->getUserIdUserCreator()->getEmail())
            ->subject('Demande de paris validée')
            ->context([
                'user' => $userMail[0]->getUserIdUserCreator()->getFirstname(),
                'bet' => $userMail[0]->getName()
            ])
            ->htmlTemplate('admin_bet/mailConfirmPublication.html.twig');

            $mailer->send($message);

            $this->addFlash('green', 'Paris publié !');
            $this->addFlash('message', 'Un email de validation vient d\'être envoyé à l\'utilisateur !');

        }

        if($formRefuse->isSubmitted()) {
            $betToUpdate->setStatus($formRefuse['status']->getData());
            
            $this->getDoctrine()->getManager()->persist($betToUpdate);
            $this->getDoctrine()->getManager()->flush();

            $message = (new TemplatedEmail())
            
            ->from(new Address('teachr.contact.pa@gmail.com', 'Anybet'))
            ->to($userMail[0]->getUserIdUserCreator()->getEmail())
            ->subject('Demande de paris refusée')
            ->context([
                'user' => $userMail[0]->getUserIdUserCreator()->getFirstname(),
                'bet' => $userMail[0]->getName()
            ])
            ->htmlTemplate('admin_bet/mailConfirmRefuseBet.html.twig');

            $mailer->send($message);

            $this->addFlash('red', 'La demande a été refusée');
            $this->addFlash('message', 'Un email vient d\'être envoyé à l\'utilisateur !');
        }

        if($formWinChoice->isSubmitted()) {
            $betToUpdate->setWinningChoice($formWinChoice['name']->getData());
            $this->getDoctrine()->getManager()->persist($betToUpdate);
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('green', 'Choix gagnant enregistré !');
            
            /* 
            Envoi des mails et envoi d'argent aux gagnants 
            
            */  
            $id_winning_choice = $formWinChoice['name']->getData()->getId();
            $winning_users =  $betChoiceHasUser->findBy(["id_betChoice"=>$id_winning_choice]);
            
            foreach($winning_users as $winning_user) {
                if($winning_user->getValidated() === true) {
                    $gain = $winning_user->getStake() * $winning_user->getRatingAtThisMoment();

                    $userMail = $winning_user->getUserIdUser()->getEmail();
                    $message = (new TemplatedEmail())
                    ->from(new Address('teachr.contact.pa@gmail.com', 'Anybet'))
                    ->to($userMail)
                    ->subject('Vous avez gagné le pari !')
                    ->context([
                        'user' => $winning_user->getUserIdUser()->getFirstname(),
                        'bet_name' =>  $winning_user->getIdBetChoice()->getBet()->getName(),
                        'bet_choice' => $winning_user->getIdBetChoice()->getName(),
                        'gain' => $gain
                    ])
                    ->htmlTemplate('winners/mailWinningUsers.html.twig');

                    $mailer->send($message);

                    // Transfert d'argent aux gagnants
                    
                    $total = $winning_user->getUserIdUser()->getWallet()->getTotal();
                    $wallet = $winning_user->getUserIdUser()->getWallet()->setTotal($total + $gain);

                    //Transaction
                    $transaction = new Transaction;
                    $transaction->setWalletIdWallet($winning_user->getUserIdUser()->getWallet());
                    $transaction->setAmount($gain);
                    $transaction->setReason("Gain pour le pari " . $winning_user->getIdBetChoice()->getBet()->getName());
                    date_default_timezone_set('Europe/Paris');
                    $transaction->setDateTime(new \DateTime('now'));

                    $manager->persist($transaction, $wallet);
                    $manager->flush();

                }
                
            }

            $this->addFlash('green', "Les emails et les transferts d'argent ont bien été envoyés aux gagnants");

            // Calcul du gain pour le créateur du pari
            $totalStake = $betToUpdate->getTotalStake();
            if ( $totalStake >= 200){
                $creatorEmail = $betToUpdate->getUserIdUserCreator()->getEmail();                
                $gainCreator = $totalStake * (5/100);
                $message = (new TemplatedEmail())
                    ->from(new Address('teachr.contact.pa@gmail.com', 'Anybet'))
                    ->to($creatorEmail)
                    ->subject('Gain pour la création de votre pari ' . $betToUpdate->getName() . '!')
                    ->context([
                        'user' => $betToUpdate->getUserIdUserCreator()->getFirstname(),
                        'bet_name' =>  $betToUpdate->getName(),
                        'gain' => $gainCreator
                    ])
                    ->htmlTemplate('winners/mailBetCreator.html.twig');
        
                $mailer->send($message);
                
                 // Transfert d'argent aux gagnants
                    
                $total = $betToUpdate->getUserIdUserCreator()->getWallet()->getTotal();
                $wallet = $betToUpdate->getUserIdUserCreator()->getWallet()->setTotal($total + $gainCreator);
                 
                 //Transaction
                 $transaction = new Transaction;
                 $transaction->setWalletIdWallet($betToUpdate->getUserIdUserCreator()->getWallet());
                 $transaction->setAmount($gainCreator);
                 $transaction->setReason("Gain pour la création du pari " . $betToUpdate->getName());
                 date_default_timezone_set('Europe/Paris');
                 $transaction->setDateTime(new \DateTime('now'));

                 $manager->persist($transaction, $wallet);
                 $manager->flush();
                    
                $this->addFlash('green', "L'email et le transfert d'argent ont bien été effectués pour le créateur du pari");
            }

            // Calcul du gain pour le site
            $gainSite = $totalStake * (5/100);
            if ($gainSite > 0) {
                $users_admin = $user->findByRole("ROLE_ADMIN");
                $first_user_admin = $users_admin[0];
                $wallet = $first_user_admin->getWallet();
                $wallet->setTotal($gainSite);

                // Transaction
                $transaction = new Transaction;
                $transaction->setWalletIdWallet($wallet);
                $transaction->setAmount($gainSite);
                $transaction->setReason("Gain du site Anybet pour le  pari " . $betToUpdate->getName());
                date_default_timezone_set('Europe/Paris');
                $transaction->setDateTime(new \DateTime('now'));

                $manager->persist($transaction, $wallet);
                $manager->flush();
            }
            
        }

        $this->faker = \Faker\Factory::create("fr_FR");
        $bet = $betRepo->findOneBy(['id'=>$id]);
        $event = $eventRepo->findOneBy(["id"=>$bet->getEventIdEvent()]);
        $interval = $event->getDate()->diff(new \DateTime("now"));
        $elapsed = $interval->format('%a jours %h heures %i minutes');
        
        return $this->render('admin_bet/show.html.twig', [
            'bet' => $bet,
            'betChoices' => $betChoices->findBy(["bet"=>$id]),
            'faker' => $this->faker,
            'event' => $event,
            'elapsed' => $elapsed,
            'user' => $user->findOneBy(['id'=>$bet->getUserIdUserCreator()]),
            'formValidate' => $formValidate->createView(),
            'formRefuse' => $formRefuse->createView(),
            'formWinChoice' => $formWinChoice->createView(),
            'isPublic' => $isPublic
        ]);
    }

    #[Route('/{id}/edit', name: 'admin_bet_edit', methods: ['GET', 'POST'])]
    #[IsGranted(AdminVoter::EDIT, 'bet')]
    public function edit(Request $request, Bet $bet): Response
    {
        $this->denyAccessUnlessGranted(AdminVoter::EDIT, $bet);

        $form = $this->createForm(BetType::class, $bet)->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute("admin_bet_index");
        }

        return $this->render('admin_bet/edit.html.twig', [
            "bet" => $bet,
            "form" => $form->createView(),

        ]);

    }

    #[Route('/{id}/delete', name: 'admin_bet_delete', methods: ['POST'])]
    #[IsGranted(AdminVoter::DELETE, 'bet')]
    public function delete(Request $request, Bet $bet, EntityManagerInterface $entityManager, BetChoiceRepository $betChoiceRepository, $id, BetRepository $betRepository): Response
    {
        $this->denyAccessUnlessGranted(AdminVoter::DELETE, $bet);

        if ($this->isCsrfTokenValid('delete'.$bet->getId(), $request->request->get('_token'))) {
    
            $betToUpdate = $betRepository->findBy(['id' => $id]);
            
            foreach($betToUpdate as $bets){
                $bets->setWinningChoice(null);
                $entityManager->flush();
            }
        
            $entityManager->remove($bet);
            
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin_bet_index', [], Response::HTTP_SEE_OTHER);
    }
}
