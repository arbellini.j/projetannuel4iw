<?php

namespace App\Controller\Back;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    #[Route('/', name: 'default')]
    public function index(): Response
    {

        // return $this->render('admin_category/index.html.twig', [
        //     'controller_name' => 'DefaultController',
        // ]);

        return $this->redirectToRoute('admin_category_index');


    }
}
