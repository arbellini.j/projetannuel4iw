<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Wallet;
use App\Form\User1Type;
use App\Repository\BetRepository;
use App\Form\EditRoleUserType;
use App\Manager\WalletManager;
use App\Security\EmailVerifier;
use App\Repository\UserRepository;
use Symfony\Component\Mime\Address;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use App\Security\Voter\AdminVoter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

#[Route('/admin/gestion-utilisateur')]
class AdminUserManagementController extends AbstractController
{   
    private EmailVerifier $emailVerifier;

    public function __construct(EmailVerifier $emailVerifier)
    {
        $this->emailVerifier = $emailVerifier;
    }


    #[Route('/', name: 'app_user_management_index', methods: ['GET'])]
    #[IsGranted('ROLE_ADMIN')]
    public function index(UserRepository $userRepository): Response
    {   
        return $this->render('user_management/index.html.twig', [
            'users' => $userRepository->findAll(),
        ]);
    }

    #[Route('/nouveau', name: 'app_user_management_new', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_ADMIN')]
    public function new(Request $request, EntityManagerInterface $entityManager, UserPasswordHasherInterface $userPasswordHasherInterface, WalletManager $walletManager): Response
    {
        $user = new User();
        $form = $this->createForm(User1Type::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $now = new \DateTime('now');
            $user->setPassword(
            $userPasswordHasherInterface->hashPassword(
                    $user,
                    $form->get('password')->getData()
                )
            );

            $user->setDateOfBirth(
                $form->get('dateOfBirth')->getData()
            );

            $difference = $now->diff($form->get('dateOfBirth')->getData());
            $age = $difference->y;
            
            if($age >= 18) {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($user);
                $entityManager->flush();
                
                $this->addFlash('success', 'l\'utilisateur a bien été créé !' );
            
                // generate a signed url and email it to the user
                $this->emailVerifier->sendEmailConfirmation('app_verify_email', $user,
                (new TemplatedEmail())
                    ->from(new Address('teachr.contact.pa@gmail.com', 'Anybet'))
                    ->to($user->getEmail())
                    ->subject('Confirmation de votre adresse mail')
                    ->htmlTemplate('registration/confirmation_email.html.twig')
                    ->context([
                        'userFirstname' => $user->getFirstname()
                    ])
            );

            // Création wallet pour le nouvel user
            $wallet = new Wallet;
            $total = 0;
            $walletManager->chargeWallet($wallet, $user, $total);
            
            return $this->redirectToRoute('app_user_management_index', [], Response::HTTP_SEE_OTHER);

            } else {
                echo "<p class='no-entry-text'>Attention vous êtes mineur, l'accès à ce site vous est interdit.</p>";
            }
            
        }

        return $this->renderForm('user_management/new.html.twig', [
            'user' => $user,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_user_management_show', methods: ['GET'])]
    #[IsGranted(AdminVoter::VIEW, 'user')]
    public function show(User $user): Response
    {
        $this->denyAccessUnlessGranted(AdminVoter::VIEW, $user);

        return $this->render('user_management/show.html.twig', [
            'user' => $user,
        ]);
    }

    #[Route('/{id}/modifier', name: 'app_user_management_edit', methods: ['GET', 'POST'])]
    #[IsGranted(AdminVoter::EDIT, 'user')]
    public function edit(Request $request, User $user, EntityManagerInterface $entityManager): Response
    {
        $this->denyAccessUnlessGranted(AdminVoter::EDIT, $user);

        $form = $this->createForm(EditRoleUserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setRoles($form->get('roles')->getData());
            $entityManager->persist($user);
            $entityManager->flush();
            $this->addFlash('success', 'Le role de l\'utilisateur a bien été mis à jour');

            return $this->redirectToRoute('app_user_management_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('user_management/edit.html.twig', [
            'user' => $user,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_user_management_delete', methods: ['POST'])]
    #[IsGranted(AdminVoter::DELETE, 'user')]
    public function delete(Request $request, User $user, EntityManagerInterface $entityManager, BetRepository $betRepository, UserRepository $userRepository,$id): Response
    {
        $this->denyAccessUnlessGranted(AdminVoter::DELETE, $user);
        
        if ($user->getId() === $this->getUser()->getId()) {
            $this->addFlash('red', 'Vous ne pouvez pas supprimer votre propre compte !');
            return $this->redirectToRoute('app_user_management_index', [], Response::HTTP_SEE_OTHER);
        } else {
            if ($this->isCsrfTokenValid('delete'.$user->getId(), $request->request->get('_token'))) {
                $adminId = $this->getDoctrine()
                ->getRepository(User::class)
                ->findAdminId();

                $betsToSet = $betRepository->findBy(['user_idUser_creator' => $id]);

                foreach($betsToSet as $bets){
                    $bets->setUserIdUserCreator($adminId[0]);
                }
                $entityManager->remove($user);
                $entityManager->flush();
                $this->addFlash('success', 'L\'utilisateur a bien été supprimé !' );
            }
        }
        
        return $this->redirectToRoute('app_user_management_index', [], Response::HTTP_SEE_OTHER);
    }
}
