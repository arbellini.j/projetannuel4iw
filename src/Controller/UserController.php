<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\BetRepository;
use App\Repository\BetChoiceHasUserRepository;
use App\Repository\UserRepository;
use App\Form\UserType;
use App\Form\UserEditPasswordType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\Session;
use App\Security\Voter\UserVoter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

#[Route('/user')]
class UserController extends AbstractController
{
    #[Route('/{id}/edit', name: 'user_edit', methods: ['GET', 'POST'])]
    #[IsGranted(UserVoter::EDIT, 'user')]
    public function edit(Request $request, User $user, EntityManagerInterface $entityManager, UserPasswordHasherInterface $userPasswordHasherInterface): Response
    {
        $this->denyAccessUnlessGranted(UserVoter::EDIT, $user);

        $form = $this->createForm(UserType::class, $user);
        $form_password = $this->createForm(UserEditPasswordType::class, $user);
        $form->handleRequest($request);
        $form_password->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();
        }

        if ($form_password->isSubmitted() && $form_password->isValid()) {
            $user->setPassword(
            $userPasswordHasherInterface->hashPassword(
                    $user,
                    $form_password->get('password')->getData()
                )
            );
            $entityManager->flush();
        }
        if($this->getUser() != null){
            return $this->renderForm('user/edit.html.twig', [
                'user' => $user,
                'form' => $form,
                'form_password' => $form_password,
            ]);
        }
        else{
            return $this->redirectToRoute("app_login");
        }

    }

    #[Route('/{id}', name: 'user_delete', methods: ['POST'])]
    #[IsGranted(UserVoter::DELETE, 'user')]
    public function delete(Request $request, User $user, EntityManagerInterface $entityManager, BetRepository $betRepository, UserRepository $userRepository, BetChoiceHasUserRepository $betChoiceHasUserRepository, $id): Response
    {
        $this->denyAccessUnlessGranted(UserVoter::DELETE, $user);
        
        if ($this->isCsrfTokenValid('delete'.$user->getId(), $request->request->get('_token'))) {
            $currentUserId = $this->getUser()->getId();
            if ($currentUserId == $user->getId())
            {
                $session = $this->get('session');
                $session = new Session();
                $session->invalidate();
            }

            $adminId = $this->getDoctrine()
            ->getRepository(User::class)
            ->findAdminId();
            
            $betsToSet = $betRepository->findBy(['user_idUser_creator' => $id]);
            $betsHasUserToSet = $betChoiceHasUserRepository->findBy(['user_idUser' => $id]);

            foreach($betsHasUserToSet as $betHasUser) 
            {
                $entityManager->remove($betHasUser);
            }

            foreach($betsToSet as $bets){
                $bets->setUserIdUserCreator($adminId[0]);
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($user);
            $entityManager->flush();
        }
        return $this->redirectToRoute('app_login');
    }
}
