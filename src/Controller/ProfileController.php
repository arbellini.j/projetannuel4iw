<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use App\Repository\BetRepository;
use App\Repository\BetChoiceHasUserRepository;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mime\Address;
use App\Entity\Bet;
use App\Entity\BetChoiceHasUser;
use App\Form\InviteUserPrivateBetType;
use App\Repository\BetChoiceRepository;
use App\Repository\TransactionRepository;
use App\Repository\WalletRepository;
use Doctrine\ORM\EntityManagerInterface;

#[Route('/profil')]
class ProfileController extends AbstractController
{
    #[Route('/mon-compte', name: 'app_profile_account')]
    public function index(): Response
    {
        if($this->getUser() != null){

            return $this->render('profile/account.html.twig', [
                
            ]);
        }  
        else{
            return $this->redirectToRoute("app_login");
        }
    }

    #[Route('/recharge-cagnotte', name: 'app_profile_wallet')]
    public function wallet(): Response
    {
        if($this->getUser() != null){
            return $this->render('profile/wallet.html.twig', [
            ]);
        }
        else{
            return $this->redirectToRoute("app_login");
        }
    }

    #[Route('/statistiques-du-compte', name: 'app_profile_stat')]
    public function account_stat(BetRepository $betRepository, TransactionRepository $transactionRepository, EntityManagerInterface $em): Response
    {
        if($this->getUser() != null){
            // Graph pari normaux vs pari premium
            $normalBets = $betRepository->findBy(['isSponsored'=>'false', 'user_idUser_creator' => $this->getUser()->getId()]);
            $premiumBets = $betRepository->findBy(['isSponsored'=>'true', 'user_idUser_creator' => $this->getUser()->getId()]);

            $countNormalBets = [count($normalBets)];
            $countPremiumBets = [count($premiumBets)];
            $graphValuesPieTypeBets = array_merge($countNormalBets, $countPremiumBets);

            //Graph pour comparer le nombre de paris privés et le nombre de paris publics
            $privateBets = $betRepository->findBy(['isPublic'=>'false', 'user_idUser_creator' => $this->getUser()->getId()]);
            $publicBets = $betRepository->findBy(['isPublic'=>'true', 'user_idUser_creator' => $this->getUser()->getId()]);

            $countPrivateBets = [count($privateBets)];
            $countPublicBets = [count($publicBets)];
            
            $graphValuesPieIsPublicBet = array_merge($countPrivateBets, $countPublicBets);

            //Graph pour comparer les paris ayant différents statuts
            $validatedBets = $betRepository->findBy(['status'=>'VALIDÉ', 'user_idUser_creator' => $this->getUser()->getId()]);
            $endedBets = $betRepository->findBy(['status'=>'Terminé', 'user_idUser_creator' => $this->getUser()->getId()]);
            $refusedBets = $betRepository->findBy(['status'=>'REFUSÉ', 'user_idUser_creator' => $this->getUser()->getId()]);
            $inProgressBets = $betRepository->findBy(['status'=>'EN COURS', 'user_idUser_creator' => $this->getUser()->getId()]);
            
            $countValidatedBets = [count($validatedBets)];
            $countEndedBets = [count($endedBets)];
            $countRefusedBets = [count($refusedBets)];
            $countInProgressBets = [count($inProgressBets)];
    
            $graphValuesPieBetStatus = array_merge($countValidatedBets, $countEndedBets, $countRefusedBets, $countInProgressBets);

            //Graph linéaire pour connaître les gains sur les paris et sur la création de pari
            $betGains = $transactionRepository->betGain("Gain pour le pari%", $this->getUser()->getId());

            if (! empty($betGains)) {
                foreach($betGains as $betGain){
                    $dates[] = $betGain['date_gain'];
                    $betGainPerDate[] = $betGain['amount'];
                }
            } else {
                $dates = [];
                $betGainPerDate = [];
            }

            $creationBetGains = $transactionRepository->betGain("Gain pour la création du pari%", $this->getUser()->getId());
            if (! empty($creationBetGains)) {
                foreach($creationBetGains as $creationBetGain){
                  
                    $datesCreationBetGain[] = $creationBetGain['date_gain'];
                    $creationBetGainPerDate[] = $creationBetGain['amount'];
                   
                }

            } else {
                $datesCreationBetGain = [];
                $creationBetGainPerDate = [];
            }
            
            return $this->render('profile/account-stat.html.twig', [
                'countTypeBets' => json_encode($graphValuesPieTypeBets),
                'countIsPublicBet' => json_encode($graphValuesPieIsPublicBet),
                'countBetStatus' => json_encode($graphValuesPieBetStatus),
                'betGainPerDate' => json_encode($betGainPerDate),
                'datesBetGain' => json_encode($dates),
                'creationBetGainPerDate' => json_encode($creationBetGainPerDate),
                'dateCreationBetGain' => json_encode($datesCreationBetGain)
            ]);
        }
        else{
            return $this->redirectToRoute("app_login");
        }
    }

    #[Route('/historique-des-transactions', name: 'app_profile_transaction')]
    public function transaction(TransactionRepository $transactionRepository, WalletRepository $walletRepository): Response
    {
        $currentUser = $this->getUser()->getId();
        $userWallet = $walletRepository->findBy(['user_idUser'=>$currentUser]);
        $userWalletId = $userWallet[0]->getId();
        $transactions = $transactionRepository->findBy(['wallet_idWallet'=>$userWalletId], ["dateTime"=>"DESC"]);

        if($this->getUser() != null){
            return $this->render('profile/transaction.html.twig', [
                'transactions' => $transactions
            ]);
        }
        else{
            return $this->redirectToRoute("app_login");
        }

    }

    #[Route('/mes-paris', name: 'app_profile_my_bets')]
    public function bets(BetRepository $betRepository, BetChoiceHasUserRepository $betChoiceAsUserRepo, BetChoiceRepository $betChoiceRepo): Response
    {
        if($this->getUser() != null){
            $user= $this->getUser();
            $id= $user->getId();

            $betParticipationList = $betChoiceAsUserRepo->getUserParticipationBet($id);

            return $this->render('profile/my-bets.html.twig', [
                'bets' => $betRepository->findBy(['user_idUser_creator'=>$id]),
                'betParticipationList' => $betParticipationList
            ]);
        }
        else{
            return $this->redirectToRoute("app_login");
        }
    }

    #[Route('/pari/{id}', name: 'app_profile_bet', methods: ['GET','POST'])]
    public function showPrivateBet(Request $request, Bet $bet, BetChoiceRepository $betChoices, BetChoiceHasUserRepository $AsUser, MailerInterface $mailer, TokenGeneratorInterface $tokenGenerator, $id): Response
    {
        
        if ($this->getUser() != null) {
            $user = $this->getUser();
            $wallet = $user->getWallet();
            setcookie('wallet_amount', $wallet->getTotal(), ['secure' => false, 'samesite' => 'None' ]);
        }
        else {
            $user = "";
        }

        $formInviteUser = $this->createForm(InviteUserPrivateBetType::class);
        $formInviteUser->handleRequest($request);
        $id_event = $bet->getEventIdEvent();
        $betChoices = $betChoices->findBy(['bet' => $id]);
        $userDistinct = $this->getDoctrine()
            ->getRepository(BetChoiceHasUser::class)
            ->findUserDistinct($id);

        $stakes = $this->getDoctrine()
            ->getRepository(BetChoiceHasUser::class)
            ->findTotalStakePerBet($id);

        $totalStake = "";
        foreach($stakes as $stake) {
            $totalStake = floatval($totalStake) + floatval($stake["stake"]);
        }

        $userCreator = $bet->getUserIdUserCreator()->getId();
        $token = $tokenGenerator->generateToken();
        $urlBet = $this->generateUrl('app_bet_user_invite', array('id' => $id, 'token' => $token), UrlGeneratorInterface::ABSOLUTE_URL); 

        if ($formInviteUser->isSubmitted() && $formInviteUser->isValid()) {
            $inviteUserMail = $formInviteUser->get('email')->getData();
                   
                $message = (new TemplatedEmail())
                ->from(new Address('teachr.contact.pa@gmail.com', 'Anybet'))
                ->to($inviteUserMail)
                ->subject('Invitation à un pari')
                ->htmlTemplate('profile/invite_user_private_bet.html.twig')
                ->context([
                    'userFirstname' => $user->getFirstname(),
                    'userLastname' => $user->getLastname(),
                    'urlBet' => $urlBet,
                ]);

                $mailer->send($message);

                $this->addFlash('message', 'Le mail d\'invitation a été envoyé !');

                return $this->redirect($request->getUri());
        } 

        $allStakes = $AsUser->findBy(['validated'=>'false']);
        // $allStakes = $allStakes
        $allStakeValues = [];
        $allGainValues = [];
        foreach($allStakes as $value ){
            array_push($allStakeValues, $value->getStake());
            array_push($allGainValues, $value->getRatingAtThisMoment()*$value->getStake());
        }
        $allStakeValues = array_sum($allStakeValues);
        $allGainValues = array_sum($allGainValues);

        return $this->render('profile/detail-bet.html.twig', [
            'formInviteUser' => $formInviteUser->createView(),
            'asUser' => $AsUser,
            'event' => $id_event,
            'bet' => $bet,
            'user' => $user,
            'betChoices' => $betChoices,
            'userDistinct' => $userDistinct,
            'userCreator' => $userCreator,
            'id' => $id,
            'sumOfStakes' => $allStakeValues,
            'sumOfGain' => $allGainValues,
            'urlBet' => $urlBet,
            'totalStake' => $totalStake,
        ]);
    }

    #[Route('/pari/{id}/{token}', name: 'app_bet_user_invite', methods: ['GET','POST'])]
    public function viewBetUserInvite(Request $request, Bet $bet, BetChoiceRepository $betChoices, BetChoiceHasUserRepository $AsUser, $id)
    {
        $user = $this->getUser();
        if ($user != null) {
            $wallet = $user->getWallet();
            setcookie('wallet_amount', $wallet->getTotal(), ['secure' => false, 'samesite' => 'None' ]);
        }

        $betChoices = $betChoices->findBy(['bet' => $id]);
        $id_event = $bet->getEventIdEvent();
        $userCreator = $bet->getUserIdUserCreator()->getId();
        $userDistinct = $this->getDoctrine()
            ->getRepository(BetChoiceHasUser::class)
            ->findUserDistinct($id);

        $stakes = $this->getDoctrine()
            ->getRepository(BetChoiceHasUser::class)
            ->findTotalStakePerBet($id);

        $totalStake = "";
        foreach($stakes as $stake) {
            $totalStake = floatval($totalStake) + floatval($stake["stake"]);
        }
            
        if(!$user) {
            return $this->redirectToRoute('app_login', array(
                'uri' => $_SERVER['REQUEST_URI'],
            ));
        }

        return $this->render('profile/detail-bet.html.twig', [
            'user' => $user,
            'asUser' => $AsUser,
            'event' => $id_event,
            'userCreator' => $userCreator,
            'bet' => $bet,
            'betChoices' => $betChoices,
            'id' => $id,
            'userDistinct' => $userDistinct,
            'totalStake' => $totalStake
        ]);
    } 
}
