<?php

namespace App\Controller;


use App\Repository\BetChoiceHasUserRepository;
use App\Repository\CategoryRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Bet;
use App\Entity\Event;
use App\Entity\User;
use App\Form\CreateBetType;
use App\Repository\EventRepository;
use App\Repository\BetRepository;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mime\Address;
use App\Entity\Transaction;


class CreateBetController extends AbstractController
{
    #[Route('/create/bet', name: 'app_create_bet', methods: ['GET', 'POST'])]
    public function index(Request $request, BetChoiceHasUserRepository $AsUser, EntityManagerInterface $entityManager, EventRepository $eventRepository, BetRepository $betRepository, MailerInterface $mailer, EntityManagerInterface $manager): Response
    {
        $bet = new Bet();
        $event = new Event();
        $user = new User();

        $form = $this->createForm(CreateBetType::class, $bet)->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $bet->setCreatedAt(new \DateTime("now"));
            $bet->setUserIdUserCreator($this->getUser());
            $bet->setIsPublic($form['isPublic']->getData());
            $betChoicesList = $bet->getBetChoices();
            foreach ($betChoicesList as $betChoice){
                $betChoice->setRating("2");
            }

            if($form['isPublic']->getData() == false){
                $bet->setStatus("PRIVÉ");
            }
            
            if($form['event']->getData() != null){
                $event->setName($form['event']->getData());
                $event->setDescription($form['eventDescription']->getData());
                $event->setDate($form['eventDate']->getData());
                $event->setStatus("EN COURS");
                $this->getDoctrine()->getManager()->persist($event);
            }
            $this->getDoctrine()->getManager()->persist($bet);
            $this->getDoctrine()->getManager()->flush();
            $lastBet = $betRepository->findBy(array(),array('id'=>'DESC'),1,0); 
            $lastEvent = $eventRepository->findBy(array(),array('id'=>'DESC'),1,0);

            if($form['isPublic']->getData() == true){

                $message = (new TemplatedEmail())
                ->from(new Address('teachr.contact.pa@gmail.com', 'Anybet'))
                ->to('teachr.contact.pa@gmail.com')
                ->subject('Nouvelle demande de paris créee !')
                ->context([
                    'id' => $bet->getId()
                ])
                ->htmlTemplate('create_bet/mailNotifyAdmin.html.twig');

                $mailer->send($message);
            }

            if($lastBet[0]->getEventIdEvent() == null){

                $bet->setEventIdEvent($lastEvent[0]);
                $event->setCategoryIdCategory($form['categoryList']->getData());

                $this->getDoctrine()->getManager()->persist($bet);
                $this->getDoctrine()->getManager()->flush();

            }
        
            return $this->redirectToRoute("default");
        }

        if($this->getUser() != null){
            $user = $this->getUser()->getId();
            return $this->render('create_bet/index.html.twig', [
                'form' => $form->createView(),
                'user'=>$user,
                'asUser'=>$AsUser,
            ]);
        }
        else{
            return $this->redirectToRoute("app_login");
        }
    }
}
