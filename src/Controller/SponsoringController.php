<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Form\CreateSponsoredBetType;
use App\Entity\Bet;
use App\Entity\Event;
use App\Repository\BetRepository;
use App\Repository\EventRepository;
use App\Repository\CategoryRepository;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mime\Address;

class SponsoringController extends AbstractController
{

    #[Route('/sponsoring', name: 'form_sponsoring_bet', methods: ['GET', 'POST'])]
    public function index(Request $request, EventRepository $eventRepository, BetRepository $betRepository, MailerInterface $mailer, CategoryRepository $categoryRepository): Response
    {

        $bet = new Bet();
        $event = new Event();
        $bool = false;

        $form = $this->createForm(CreateSponsoredBetType::class, $bet)->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            
            $user = $this->getUser();
            $wallet = $user->getWallet();
            $total_wallet = $wallet->getTotal();
            if ($total_wallet >= 80) {
            
                $_SESSION['bet_theme'] = $_POST["theme"];

                $_SESSION['bet_name'] = $form['name']->getData();

                if($form['event_idEvent']->getData() != null){
                    $_SESSION['selected_event'] = $form['event_idEvent']->getData();
                }

                $_SESSION['event_name'] = $form['event']->getData();
                $_SESSION['event_description'] = $form['eventDescription']->getData();
                $_SESSION['event_date'] = $form['eventDate']->getData();
                
                $_SESSION['bet_choices'] = $bet->getBetChoices();

                $_SESSION['idCategory'] = $form['categoryList']->getData();
                
               return $this->redirectToRoute("app_payment_sponsored_bet");
            } else {
                return $this->redirectToRoute("app_profile_wallet");
            }
        }


        // if((!isset($_SESSION['validate_payment']) && session_status() === PHP_SESSION_NONE) || (!$_SESSION['validate_payment'] && session_status() === PHP_SESSION_NONE)){
        //     session_start();
        // }

        if (isset($_SESSION['validate_payment'])){
            if ($_SESSION['validate_payment'] == true){
                $bet->setCreatedAt(new \DateTime("now"));
                $bet->setUserIdUserCreator($this->getUser());
                $bet->setIsPublic(true);
                $bet->setIsSponsored(true);
                $bet->setName($_SESSION['bet_name']);
                $bet->setColor($_SESSION['bet_theme']);
                $event->setStatus("EN COURS");

                $choiceList = $_SESSION['bet_choices'];    

                if(isset($_SESSION['selected_event'])){
                    $eventId = $_SESSION['selected_event']->getId();

                    $eventObject = $eventRepository->findBy(["id"=>$eventId]);

                    $bet->setEventIdEvent($eventObject[0]);
                }
                
                if($_SESSION['event_name']!= null){
                    $event->setName($_SESSION['event_name']);
                    $event->setDescription($_SESSION['event_description']);
                    $event->setDate($_SESSION['event_date']);
                    $this->getDoctrine()->getManager()->persist($event);
                }

                $this->getDoctrine()->getManager()->persist($bet);
                $this->getDoctrine()->getManager()->flush();

                $lastBet = $betRepository->findBy(array(),array('id'=>'DESC'),1,0); 
                $lastEvent = $eventRepository->findBy(array(),array('id'=>'DESC'),1,0);

                if($lastBet[0]->getEventIdEvent() == null){
                    
                    $bet->setEventIdEvent($lastEvent[0]);

                    if(isset($_SESSION['idCategory'])){
                        $EventCategory = $_SESSION['idCategory']->getId();
                        $categoryObject = $categoryRepository->findBy(["id"=>$EventCategory]);
                        
                        $event->setCategoryIdCategory($categoryObject[0]);
                       
                    }

                    $this->getDoctrine()->getManager()->persist($bet);
                    $this->getDoctrine()->getManager()->flush();

                    
                }

                foreach ($choiceList as $betChoice){

                    $betChoice->setRating("2");
                    $betChoice->setBet($bet);
                    
                    $this->getDoctrine()->getManager()->persist($betChoice);
                }
            
                $this->getDoctrine()->getManager()->flush();

                $_SESSION['validate_payment'] = false;
                unset($_SESSION['selected_event']);
                unset($_SESSION['bet_choices']);

                $bool = true;
                $this->addFlash('message', 'Votre demande à bien été prise en compte');

                $messageToAdmin = (new TemplatedEmail())
                ->from(new Address('teachr.contact.pa@gmail.com', 'Anybet'))
                ->to('teachr.contact.pa@gmail.com')
                ->subject('Demande de pari sponsorisé')
                ->context([
                    'id' => $bet->getId()
                ])
                ->htmlTemplate('sponsoring/mailNotifyAdmin.html.twig');

                $mailer->send($messageToAdmin);

                $messageToUser = (new TemplatedEmail())
                ->from(new Address('teachr.contact.pa@gmail.com', 'Anybet'))
                ->to($bet->getUserIdUserCreator()->getEmail())
                ->subject('Reçu demande pari sponsorisé')
                ->context([
                    'firstname' => $bet->getUserIdUserCreator()->getFirstname(),
                    'lastname' => $bet->getUserIdUserCreator()->getLastname(),
                    'betName' => $bet->getName(),
                    'betEventName' => $bet->getEventIdEvent()->getName(),
                    'betEventDate' => $bet->getEventIdEvent()->getDate(),
                ])
                ->htmlTemplate('sponsoring/mailReceiptUser.html.twig');

                $mailer->send($messageToUser);

                $this->addFlash('message', 'Un email de confirmation vient d\'être envoyé !');
            }
        }

        
        if($this->getUser() != null){
            return $this->render('sponsoring/index.html.twig', [
                'form' => $form->createView(),
                'validate_payment' => $bool
            ]);
        }
        else{
            return $this->redirectToRoute("app_login");
        }
    }

    #[Route('/confirmPaymentSponso', name: 'app_confirm_payment_sponso', methods: ['GET', 'POST'])]
    public function confirmPaymentSponso(): Response
    {
        return $this->render('sponsoring/modalConfirmPaymentSponso.html.twig');
    }

}
