<?php

namespace App\Controller;

use App\Entity\Event;
use App\Entity\Category;
use App\Entity\Bet;
use App\Form\EventType;
use App\Form\AdminResetFilterType;
use App\Form\AdminEventFilterType;
use App\Repository\BetRepository;
use App\Repository\EventRepository;
use App\Repository\UserRepository;
use App\Repository\CategoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Security\Voter\AdminVoter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

#[Route('/admin/event')]
class AdminEventController extends AbstractController
{
    #[Route('/', name: 'admin_event_index', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_ADMIN')]
    public function index(EventRepository $eventRepository, Request $request): Response
    {

        $formFilter = $this->createForm(AdminEventFilterType::class)->handleRequest($request);
        $formReset = $this->createForm(AdminResetFilterType::class)->handleRequest($request);

        if ($formFilter->isSubmitted()) {
            if($formFilter['status']->getData() == "EN COURS" || $formFilter['status']->getData() == "Terminé"){
                $data = $eventRepository->findBy(
                    [
                        "status" => $formFilter['status']->getData()
                    ],
                    [
                        "date" => "DESC"
                    ]

                );
            }
            
            else{
                $data = $eventRepository->findBy([], ["date" => "DESC"]);
            }
        }
        else{
            $data = $eventRepository->findBy([], ["date" => "DESC"]);
        }

        if ($formReset->isSubmitted()) {
            $data = $eventRepository->findBy([], ["date" => "DESC"]);
        }

        return $this->render('admin_event/index.html.twig', [
            'events' => $data,
            'formFilter' => $formFilter->createView(),
            'formReset' => $formReset->createView()
        ]);
    }

    #[Route('/new', name: 'admin_event_new', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_ADMIN')]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $event = new Event();
        $form = $this->createForm(EventType::class, $event);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $event->setStatus("EN COURS");
            $entityManager->persist($event);
            $entityManager->flush();

            return $this->redirectToRoute('admin_event_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin_event/new.html.twig', [
            'event' => $event,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'admin_event_show', methods: ['GET'])]
    #[IsGranted(AdminVoter::VIEW, 'event')]
    public function show(Event $event, BetRepository $bet, CategoryRepository $cat, $id ): Response
    {
        $this->denyAccessUnlessGranted(AdminVoter::VIEW, $event);

        return $this->render('admin_event/show.html.twig', [
            'event' => $event,
            'id' => $id,
            'bets' => $bet->findBy(['event_idEvent'=>$id]),
            'category' => $cat->findBy(['id'=>$event->getCategoryIdCategory()])
        ]);
    }

    #[Route('/{id}/edit', name: 'admin_event_edit', methods: ['GET', 'POST'])]
    #[IsGranted(AdminVoter::EDIT, 'event')]
    public function edit(Request $request, Event $event, EntityManagerInterface $entityManager): Response
    {
        $this->denyAccessUnlessGranted(AdminVoter::EDIT, $event);

        $form = $this->createForm(EventType::class, $event);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('admin_event_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin_event/edit.html.twig', [
            'event' => $event,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'admin_event_delete', methods: ['POST'])]
    #[IsGranted(AdminVoter::DELETE, 'event')]
    public function delete(Request $request, Event $event, EntityManagerInterface $entityManager, $id, BetRepository $betRepository): Response
    {
        $this->denyAccessUnlessGranted(AdminVoter::DELETE, $event);

        if ($this->isCsrfTokenValid('delete'.$event->getId(), $request->request->get('_token'))) {
            $betsToSet = $betRepository->findBy(['event_idEvent' => $id]);
            
            foreach($betsToSet as $bets){
                $bets->setEventIdEvent(null);
            }

            $entityManager->remove($event);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin_event_index', [], Response::HTTP_SEE_OTHER);
    }
}
