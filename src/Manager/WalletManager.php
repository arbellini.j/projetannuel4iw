<?php

namespace App\Manager;

use App\Entity\User;
use App\Entity\Wallet;
use App\Services\StripeService;
use Doctrine\ORM\EntityManagerInterface;

class WalletManager {
    protected $em;

    protected $stripeService;

    public function __construct(EntityManagerInterface $entityManager, StripeService $stripeService) {
        $this->em = $entityManager;
        $this->stripeService = $stripeService;
    }

    public function intentSecret() {
        $intent = $this->stripeService->paymentIntent();

        return $intent['client_secret'] ?? null;
    }

    public function stripe(array $stripeParameter) {
        $resource = null;
        $data = $this->stripeService->stripe($stripeParameter);

        if($data) {
            $resource = [
                'stripeToken' => $data['client_secret']
            ];
        }

        return $resource;
    }


    public function chargeWallet(Wallet $wallet, User $user, $total) {
        
        $wallet->setUserIdUser($user);
        $wallet->setTotal($total/100);
        $user->setWallet($wallet);
        $this->em->persist($wallet, $user);
        $this->em->flush();

    }
}