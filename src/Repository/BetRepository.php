<?php

namespace App\Repository;

use App\Entity\Bet;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use \PDO;

/**
 * @method Bet|null find($id, $lockMode = null, $lockVersion = null)
 * @method Bet|null findOneBy(array $criteria, array $orderBy = null)
 * @method Bet[]    findAll()
 * @method Bet[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BetRepository extends ServiceEntityRepository
{
    private $pdo;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Bet::class);
        $this->pdo = new \PDO('pgsql:dbname=tikero;host=db;port=5432', 'tikero', 'password');
    }

    public function searchBet($keyWord){

        if($keyWord != null){

            $word = explode(" ", $keyWord);

            $word1 = $word[0];

            $word2 = " ";

            $word3 = " ";

            $regex = "%";
            $regex2 = "";
            $regex3 = "";

            $arrayLength = sizeof($word);

            if ($arrayLength == 2){
                $word2 = $word[1];
                $regex2 = "%";
            }
            if($arrayLength == 3){
                $word2 = $word[1];
                $word3 = $word[2];
                $regex2 = "%";
                $regex3 = "%";
            }
            
            $query = $this->pdo->prepare("SELECT name, id
                                          FROM bet
                                          WHERE name
                                          ILIKE '".$regex.$word1.$regex."' AND status = 'VALIDÉ' AND is_public = true
                                          OR name ILIKE '".$regex2.$word2.$regex2."' AND status = 'VALIDÉ' AND is_public = true
                                          OR name ILIKE '".$regex3.$word3.$regex3."' AND status = 'VALIDÉ' AND is_public = true
                                          ");
           
            $query->execute();
            $data = $query->fetchall();
            return $data;
        }

    }

    public function countByDate(){
        
        $query = $this->pdo->prepare("SELECT count(*),
                                      SUBSTRING(to_char(created_at, 'YYYY-MM-DD HH:MM:SS'), 1, 10) AS date_paris
                                      FROM bet
                                      GROUP BY date_paris
                                      ORDER BY date_paris ASC
                                    ");
        $query->execute();
        $data = $query->fetchall();
        return $data;
    }

    public function findCreateBetPerUser($id) {

        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery(
            'SELECT bet.id, COUNT(DISTINCT bchu.user_idUser), IDENTITY(bet.user_idUser_creator)
            FROM App\Entity\User usr, App\Entity\Bet bet, App\Entity\BetChoice bc, App\Entity\BetChoiceHasUser bchu
            WHERE bet.user_idUser_creator = usr.id
            AND bet.status = :status
            AND bet.isPublic = :isPublic
            AND bet.totalStake >= 300 
            AND bet.id = bc.bet
            AND bc.id = bchu.id_betChoice
            AND usr.id = '.$id.'
            GROUP BY bet.id
            HAVING COUNT(bchu.user_idUser) = 150'
        )
        ->setParameters([
            'status' => 'VALIDÉ',
            'isPublic' => 'true'
        ]);
        ;
        return $query->getResult();
    }

}
