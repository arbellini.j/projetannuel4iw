<?php

namespace App\Repository;

use App\Entity\BetChoiceHasUser;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityRepository;
use App\Repository\BetChoiceRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use \PDO;

/**
 * @method BetChoiceHasUser|null find($id, $lockMode = null, $lockVersion = null)
 * @method BetChoiceHasUser|null findOneBy(array $criteria, array $orderBy = null)
 * @method BetChoiceHasUser[]    findAll()
 * @method BetChoiceHasUser[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BetChoiceHasUserRepository extends ServiceEntityRepository
{

    private $pdo;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BetChoiceHasUser::class);
        $this->pdo = new \PDO('pgsql:dbname=tikero;host=db;port=5432', 'tikero', 'password');
    }

    // /**
    //  * @return BetChoiceHasUser[] Returns an array of BetChoiceHasUser objects
    //  */
    public function findUserDistinct($id)
    {
        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery(
            'SELECT DISTINCT u.id, u.firstname, u.lastname
            FROM App\Entity\BetChoiceHasUser bchu, App\Entity\BetChoice bc, App\Entity\User u
            WHERE bchu.id_betChoice = bc.id
            AND bchu.user_idUser = u.id
            AND bc.bet = '.$id
        ); 
        return $query->getResult();
    }

    public function findTotalStakePerBet($id)
    {
        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery(
            'SELECT bchu.stake
            FROM App\Entity\BetChoiceHasUser bchu, App\Entity\BetChoice bc
            WHERE bchu.id_betChoice = bc.id
            AND bc.bet = '.$id
        );
        return $query->getResult();
    }

    public function findParticipeBetPerUser($id) 
    {
        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery(
            'SELECT COUNT(DISTINCT b.id), IDENTITY(bchu.user_idUser)
            FROM App\Entity\User usr, App\Entity\BetChoiceHasUser bchu, App\Entity\Bet b, App\Entity\BetChoice bc
            WHERE bchu.user_idUser = usr.id
            AND bchu.id_betChoice = bc.id
            AND bc.bet = b.id
            AND usr.id = '.$id.'
            GROUP BY bchu.user_idUser, b.id'
        );
        return $query->getResult();
    }

    public function getUserParticipationBet($id){

        $query = $this->pdo->prepare(
            "SELECT DISTINCT bc.bet_id, bchu.user_id_user_id, b.name, b.is_public, b.total_stake, b.is_sponsored, b.created_at, bchu.validated
             FROM bet_choice_has_user bchu
             INNER JOIN  bet_choice bc ON bc.id = bchu.id_bet_choice_id
             INNER JOIN bet b ON b.id = bc.bet_id
             WHERE bchu.user_id_user_id = '$id'
            "
        );

        $query->execute();
        $data = $query->fetchall();
        return $data;
    }
}
