<?php

namespace App\Entity;

use App\Repository\WalletRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=WalletRepository::class)
 */
class Wallet
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var int Wallet total
     * @ORM\Column(type="integer", precision=5, scale=2)
     */
    private $total;

    /**
     * @ORM\OneToOne(targetEntity=User::class, cascade={"persist", "remove"})s
     * @ORM\JoinColumn(name="user_id_user_id", nullable=true, onDelete="CASCADE")
     */
    private $user_idUser;

    /**
     * @ORM\OneToOne(targetEntity=User::class, mappedBy="wallet", cascade={"persist", "remove"})
     * ORM\JoinColumn(name="user_id_user_id", onDelete="CASCADE")
     */
    private $get_user_by_wallet;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getTotal(): ?int
    {
        return $this->total;
    }

    public function setTotal(int $total): self
    {
        $this->total = $total;

        return $this;
    }

    public function setStripeToken(string $stripeToken): self {
        $this->stripe_token = $stripeToken;

        return $this;
    }

    public function getStrypeId(): ?string
    {
        return $this->stripe_id;
    }

    public function setStripeId(string $stripe_id): self
    {
        $this->stripe_id = $stripe_id;

        return $this;
    }

    public function getUserIdUser(): ?User
    {
        return $this->user_idUser;
    }

    public function setUserIdUser(User $user_idUser): self
    {
        $this->user_idUser = $user_idUser;

        return $this;
    }

    public function getGetUserByWallet(): ?User
    {
        return $this->get_user_by_wallet;
    }

    public function setGetUserByWallet(User $get_user_by_wallet): self
    {
        // set the owning side of the relation if necessary
        if ($get_user_by_wallet->getWallet() !== $this) {
            $get_user_by_wallet->setWallet($this);
        }

        $this->get_user_by_wallet = $get_user_by_wallet;

        return $this;
    }
}
