<?php

namespace App\Entity;

use App\Repository\BetRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Table("bet")
 * @ORM\Entity(repositoryClass=BetRepository::class)
 */
class Bet
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=45)
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isPublic = false;

    /**
     * @ORM\Column(type="string", length=45, nullable=true)
     */
    private $status = "EN COURS";

    /**
     * @ORM\Column(type="decimal", precision=5, scale=2, nullable=true)
     */
    private $totalStake = 0;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isSponsored = false;

    /**
     * @ORM\ManyToOne(targetEntity=Event::class)
     * @ORM\JoinColumn(nullable=true)
     */
    public $event_idEvent;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(name="user_id_user_creator_id", nullable=false, onDelete="CASCADE")
     */
    public $user_idUser_creator;

    /**
     * @ORM\OneToOne(targetEntity=BetChoice::class, cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $winningChoice;

    /**
     * @var Collection<int, BetChoice>
     * @ORM\OneToMany(targetEntity=BetChoice::class, mappedBy="bet", orphanRemoval=true, cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="betChoice", nullable=true, onDelete="CASCADE")
     * @Assert\Valid()
     */

    #[Assert\Count(min : 2)]
    #[Assert\Count(max : 3)]
    #[Assert\Valid]

    private Collection $betChoices;


    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $color;

    public function __construct()
    {
        $this->betChoices = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getIsPublic(): ?bool
    {
        return $this->isPublic;
    }

    public function setIsPublic(bool $isPublic): self
    {
        $this->isPublic = $isPublic;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getTotalStake(): ?string
    {
        return $this->totalStake;
    }

    public function setTotalStake(string $totalStake): self
    {
        $this->totalStake = $totalStake;

        return $this;
    }

    public function getIsSponsored(): ?bool
    {
        return $this->isSponsored;
    }

    public function setIsSponsored(bool $isSponsored): self
    {
        $this->isSponsored = $isSponsored;

        return $this;
    }

    public function getEventIdEvent(): ?Event
    {
        return $this->event_idEvent;
    }

    public function setEventIdEvent(?Event $event_idEvent): self
    {
        $this->event_idEvent = $event_idEvent;

        return $this;
    }

    public function getUserIdUserCreator(): ?User
    {
        return $this->user_idUser_creator;
    }

    public function setUserIdUserCreator(?User $user_idUser_creator): self
    {
        $this->user_idUser_creator = $user_idUser_creator;

        return $this;
    }

    public function getWinningChoice(): ?BetChoice
    {
        return $this->winningChoice;
    }

    public function setWinningChoice(?BetChoice $winningChoice): self
    {
        $this->winningChoice = $winningChoice;

        return $this;
    }

    /**
     * @var Collection<int, BetChoice>
     * @return Collection|BetChoice[]
     */
    public function getBetChoices(): Collection
    {
        return $this->betChoices;
    }

    public function addBetChoice(BetChoice $betChoice): self
    {
        if (!$this->betChoices->contains($betChoice)) {
            $this->betChoices[] = $betChoice;
            $betChoice->setBet($this);
        }

        return $this;
    }

    public function removeBetChoice(BetChoice $betChoice): self
    {
        if ($this->betChoices->removeElement($betChoice)) {
            // set the owning side to null (unless already changed)
            if ($betChoice->getBet() === $this) {
                $betChoice->setBet(null);
            }
        }

        return $this;
    }

    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(?string $color): self
    {
        $this->color = $color;

        return $this;
    }

}
