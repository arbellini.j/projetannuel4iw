<?php

namespace App\Security;

use Psr\Container\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Http\Authenticator\AbstractLoginFormAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\CsrfTokenBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Credentials\PasswordCredentials;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\Authenticator\Passport\PassportInterface;
use Symfony\Component\Security\Http\Util\TargetPathTrait;

class AppCustomAuthenticator extends AbstractLoginFormAuthenticator
{
    use TargetPathTrait;

    public const LOGIN_ROUTE = 'app_login';

    private UrlGeneratorInterface $urlGenerator;
     /**
     * @var Security
     */
    private $security;
    

    public function __construct(UrlGeneratorInterface $urlGenerator, Security $security, ContainerInterface $container)
    {
        $this->urlGenerator = $urlGenerator;
        $this->security = $security;
        $this->container = $container;
    }

    public function setContainer(ContainerInterface $container): ?ContainerInterface
    {
        $previous = $this->container;
        $this->container = $container;

        return $previous;
    }

    /**
     * Gets a container service by its id.
     *
     * @return object The service
     */
    protected function get(string $id): object
    {
        return $this->container->get($id);
    }

    /**
     * @var ContainerInterface
     */
    protected $container;

    public function authenticate(Request $request): PassportInterface
    {
        $email = $request->request->get('email', '');

        $request->getSession()->set(Security::LAST_USERNAME, $email);

        return new Passport(
            new UserBadge($email),
            new PasswordCredentials($request->request->get('password', '')),
            [
                new CsrfTokenBadge('authenticate', $request->request->get('_csrf_token')),
            ]
        );
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
        $isverif = $this->verifAccount();
        if($isverif === false) {
            $_SESSION['isVerif'] = $isverif;
            return null;
        }

        if ($request->get('_target_path')){
            return new RedirectResponse($request->get('_target_path'));
        }

        if ($targetPath = $this->getTargetPath($request->getSession(), $firewallName)) {
            return new RedirectResponse($targetPath);
        }

        if(in_array("ROLE_ADMIN", $token->getRoleNames())){
            return new RedirectResponse($this->urlGenerator->generate('admin_category_index'));
        }

        return new RedirectResponse($this->urlGenerator->generate('front_default'));
        //throw new \Exception('TODO: provide a valid redirect inside '.__FILE__);
    }

    protected function getLoginUrl(Request $request): string
    {
        return $this->urlGenerator->generate(self::LOGIN_ROUTE);
    }

    public function verifAccount() {
        if ($this->security->getUser() != null) {
            $user_verified = $this->security->getUser()->isVerified;

            if($user_verified === false) {
                $this->get('security.token_storage')->setToken(null);
                $this->get('request_stack')->getSession()->invalidate();
                return false;
            } else {
                return true;
            }
        }
    }
}
