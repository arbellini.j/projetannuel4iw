<?php

namespace App\Security\Voter;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

class AdminVoter extends Voter
{
    public const EDIT = 'edit';
    public const VIEW = 'view';
    public const DELETE = 'delete';

    protected function supports(string $attribute, $subject): bool
    {
        return in_array($attribute, [self::EDIT, self::VIEW, self::DELETE])
            && $subject instanceof \App\Entity\Bet
            || $subject instanceof \App\Entity\Event
            || $subject instanceof \App\Entity\Category
            || $subject instanceof \App\Entity\User;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        if (!$user instanceof UserInterface) {
            return false;
        }

        switch ($attribute) {
            case self::EDIT:
                return in_array('ROLE_ADMIN', $user->getRoles());
                break;
            case self::VIEW:
                return in_array('ROLE_ADMIN', $user->getRoles());
                break;
            case self::DELETE:
                return in_array('ROLE_ADMIN', $user->getRoles());
                break;
        }

        return false;
    }
}
