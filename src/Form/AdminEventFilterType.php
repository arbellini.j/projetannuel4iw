<?php

namespace App\Form;

use App\Entity\Bet;
use App\Entity\Event;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class AdminEventFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {

        $builder

            ->add('status', ChoiceType::class, [
                'required' => true,
                'placeholder'=>"Choisir un filtre",
                'choices' =>  [
                    'Événements en cours' => 'EN COURS',
                    'Événements terminés' => 'Terminé',
                ],
                'expanded' => false,
            ])

        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Event::class,
            'validation_groups' => false,
        ]);

    }
}