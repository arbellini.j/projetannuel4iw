<?php

namespace App\Form;

use App\Entity\Wallet;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class WalletType extends AbstractType
{   
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('10', ButtonType::class,
            ['attr' => ['class' => 'coin']])
            ->add('30', ButtonType::class,
            ['attr' => ['class' => 'coin']] )
            ->add('50', ButtonType::class,
            ['attr' => ['class' => 'coin']] )
            ->add('80', ButtonType::class,
            ['attr' => ['class' => 'coin']] )
            ->add('100', ButtonType::class,
            ['attr' => ['class' => 'coin']] )
            ->add('200', ButtonType::class,
            ['attr' => ['class' => 'coin']] );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Wallet::class,
            'attr'=> ['id' => 'payment-form', 'class ' => 'payment-form']
        ]);
    }
}
