<?php

namespace App\Form;

use App\Entity\BetChoice;
use App\Entity\Bet;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\ChoiceList\ChoiceList;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use App\Repository\BetChoiceRepository;
use Symfony\Component\HttpFoundation\Request;

class AdminSetWinChoice extends AbstractType
{

    private $betChoiceRepo;

    public function __construct(BetChoiceRepository $betChoiceRepo)
    {
        $this->betChoiceRepo = $betChoiceRepo;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {

        $builder

            ->add('name', EntityType::class, [
                'class' => BetChoice::class,
                'choice_label' => 'name',
                'choices' => $this->betChoiceRepo->findBy(["bet"=>$options['id']]),
                'label' => 'Choix pour ce pari',
                'placeholder' => 'Sélectionner un choix',
                'mapped' => false,
                'required' => true,
            ])

        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => BetChoice::class,
            'id' => null,
            'validation_groups' => false,
        ]);

    }
}