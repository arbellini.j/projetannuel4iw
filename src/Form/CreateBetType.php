<?php

namespace App\Form;

use App\Entity\Bet;
use App\Entity\Event;
use App\Entity\User;
use App\Entity\BetChoice;
use App\Entity\Category;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use App\Repository\EventRepository;

use App\Form\EventType;


class CreateBetType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $user = new User();

        $builder

            ->add('name', TextType::class, [
                'label' => "Nom"
            ])

            ->add('event_idEvent', EntityType::class, [
                'class' => Event::class,
                'choice_label' => 'name',
                'placeholder' => 'Sélectionner un évènement',
                'mapped' => true,
                'required' => false,
                'label' => 'Événements',
                'query_builder' => function(EventRepository $eventRepo){
                    return $eventRepo->createQueryBuilder('s')->where('s.status = ?1')->setParameter(1, 'EN COURS');
                }
            ])

            ->add('isPublic', CheckboxType::class, [
                'required' => false,
                'label' => 'Public',
                'label_attr' => [
                    'class' => 'switch-custom'
                ]
            ])

            ->add('betChoices', CollectionType::class, [
                "entry_type" => BetChoiceType::class,
                "by_reference" => false,
                "allow_add" => true,
                "allow_delete" => true,
                "error_bubbling" => false,
            ])

            ->add('event',TextType::class, [
                'required' => false,
                'mapped' => false
            ])

            ->add('eventDescription', TextareaType::class, [
                'required' => false,
                'mapped' => false,
                'label' => 'Description',
            ])

            ->add('eventDate', DateTimeType::class, [
                'required' => false,
                'mapped' => false,
                'label' => 'Date',
                'widget' => 'single_text'
            ])

            ->add('categoryList', EntityType::class, [
                'class' => Category::class,
                'choice_label' => 'name',
                'label' => 'Catégories',
                'placeholder' => 'Sélectionner une catégorie pour l\'événement',
                'mapped' => false,
                'required' => false,
            ])

        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Bet::class,
        ]);
    }
}
