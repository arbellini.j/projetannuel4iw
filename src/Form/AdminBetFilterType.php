<?php

namespace App\Form;

use App\Entity\Bet;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class AdminBetFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {

        $builder

            ->add('status', ChoiceType::class, [
                'required' => true,
                'placeholder'=>"Choisir un filtre",
                'choices' =>  [
                    'Demandes en cours' => 'EN COURS',
                    'Demandes validées' => 'VALIDÉ',
                    'Demandes refusées' => 'REFUSÉ',
                    'Paris privés' => 'false',
                    'Paris sponsorisés' => 'true'
                ],
                'expanded' => false,
            ])

        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Bet::class,
            'validation_groups' => false,
        ]);

    }
}