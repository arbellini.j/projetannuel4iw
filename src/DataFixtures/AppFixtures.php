<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\User;
use App\Entity\Category;
use App\Entity\Event;
use App\Entity\Bet;
use App\Entity\BetChoice;
use App\Entity\Wallet;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;



class AppFixtures extends Fixture
{
    protected $faker;

    public function __construct(UserPasswordHasherInterface $hasher)
    {
        $this->hasher = $hasher;

    }

    public function load(ObjectManager $manager)
    {

        $this->faker = \Faker\Factory::create("fr_FR");
        $users = [];
        //$this->faker->addProvider(new Faker\Provider\fr_FR\Person($this->faker));

        for($i=0; $i<200; $i++){

            $user = new User();
            $wallet = new Wallet();
            $user->setWallet($wallet);
            $user->setEmail('user@user'.$i+1 .'.com');
            $user->setUpdatedAt(new \DateTimeImmutable());
            $password = $this->hasher->hashPassword($user, 'testest');
            $user->setFirstname($this->faker->firstName);
            $user->setLastname($this->faker->lastName);
            $user->setPhoneNumber($this->faker->phoneNumber);
            $user->setPassword($password);
            $user->setIsVerified(true);
            $user->setRoles(["ROLE_ADMIN"]);
            array_push($users, $user);
            //$user->setImageFile($this->faker->imageUrl(50, 50, 'cats', true, 'Faker', true));
            $manager->persist($user);
            $wallet->setUserIdUser($user);
            $wallet->setTotal((string)$this->faker->numberBetween(0, 1000));
            $manager->persist($wallet);

        }
        $cat = ['Foot', 'Politique', 'Séries', 'Internet', 'Autre' ];
        $logos = ['football.png', 'politique.png', 'series.png', 'internet.png', 'autre.png'];
        /*$test = $this->faker->dateTimeBetween('-30 years', 'now', 'UTC');*/
        $evenementList = [['PSG vs OM', 'Finale LDC'], ['Premier Tour Présientielles', 'Second Tour Présientielles'], ['Attaque des Titans', 'Euphoria'], ['Memes', 'Youtube'], ['Rap', 'Catastrophe naturelle']];
        for($i=0; $i<5; $i++){
            $category = new Category();
            $category->setName($cat[$i]);
            $category->setLogo($logos[$i]);
            $category->setCreatedAt(new \DateTimeImmutable());
            $manager->persist($category);
            for($j=0; $j<2; $j++){
                $event = new Event();
                $event->setCategoryIdCategory($category);
                $event->setName($evenementList[$i][$j]);
                $event->setDescription($this->faker->sentence(20, true));
                $event->setStatus("EN COURS");
                $event->setDate($this->faker->dateTimeBetween('+7 days', '+1 year', 'UTC'));
                $manager->persist($event);
                for($k=0; $k<5; $k++){
                    $bet = new Bet();
                    $bet->setEventIdEvent($event);
                    $bet->setUserIdUserCreator($users[$this->faker->NumberBetween(0, 19)]);
                    $bet->setCreatedAt($this->faker->dateTimeBetween('-7 days', 'now', 'UTC'));
                    $bet->setName($this->faker->word);
                    $bet->setIsPublic(true);
                    $bet->setStatus("VALIDÉ");
                    $bet->setTotalStake($this->faker->NumberBetween(0, 999));
                    $bet->setIsSponsored(false);
                    for($m=0; $m<3; $m++){
                        $betChoice = new BetChoice();
                        $betChoice->setBet($bet);
                        $betChoice->setName($this->faker->word);
                        $betChoice->setRating($this->faker->randomFloat(2, 0.10, 15));
                        $manager->persist($betChoice);
                    }
                    $bet->setWinningChoice($betChoice);
                    $manager->persist($bet);
                }
            }

        }
        $manager->flush();
    }
}
